import path from 'path'
import { defineConfig } from 'vite'
import laravel from 'laravel-vite-plugin'
import react from '@vitejs/plugin-react'
import legacy from '@vitejs/plugin-legacy'

export default defineConfig({
    plugins: [
        laravel({
            input: [
                'resources/src/pages/App/App.jsx',
                'resources/src/pages/Develop/Develop.jsx',
                'resources/src/pages/Develop/Logs/Logs.jsx',
            ],
            refresh: true,
        }),
        react(),
        legacy({ targets: ['defaults', 'not IE 11'] }),
    ],
    resolve: {
        alias: {
            'app': path.resolve(__dirname, 'resources/src/pages/App'),
            'store': path.resolve(__dirname, 'resources/src/store'),
            'utils': path.resolve(__dirname, 'resources/src/utils'),
        }
    },
    build: {
        emptyOutDir: true,
        rollupOptions: {
            output: {
                entryFileNames: 'js/[name]-[hash].js',
                chunkFileNames: 'chunk/[name]-[hash].js',
                assetFileNames: ({ name }) => {
                    if (/\.(gif|jpe?g|png|svg)$/.test(name ?? ''))
                        return 'img/[name]-[hash][extname]'
                    else if (/\.css$/.test(name ?? ''))
                        return 'css/[name]-[hash][extname]'
                    else if (/\.ico$/.test(name ?? ''))
                        return '[name]-[hash][extname]'
                    else return 'assets/[name]-[hash][extname]'
                },
            }
        },
        commonjsOptions: { transformMixedEsModules: true },
    },
})
