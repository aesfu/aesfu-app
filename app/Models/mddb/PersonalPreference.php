<?php

namespace App\Models\mddb;

use Illuminate\Database\Eloquent\Model;

class PersonalPreference extends Model
{
    protected $table = 'personalpreference';
    protected $primaryKey = 'id';
    public $timestamps = false;

    protected $fillable = [
        'answer_id',
        'edelement_id',
        'competence_id',
        'task_id',
        'connectionType',
        'compName',
        'compShort',
        'compCode',
        'edelemName',
        'edelemShort',
    ];

    protected $hidden = [];
    
    /**
     * Связь многие к одному с Answer
     */
    public function answer(){
        return $this->belongsTo(Answer::class,'answer_id', 'id' );
    }

    public function edelement(){
        return $this->belongsTo(EdElement::class,'edelement_id', 'id' );
    }
}