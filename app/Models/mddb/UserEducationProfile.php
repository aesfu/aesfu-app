<?php

namespace App\Models\mddb;

use Illuminate\Database\Eloquent\Model;

class UserEducationProfile extends Model{
    protected $table = 'usereducationprofile'; 
    public $timestamps = false;

    protected $fillable = [
        'user_id',
        'edelement_id',
        'started_at',
        'finished_at',
    ];

    protected $hidden = [

    ];

    public function userEducationCourses(){
        return $this->hasMany(UserEducationCourse::class, 'usereducationprofile_id')->with('educationcourse')->with('edelement');
    }

    public function userDigitalFootPrints(){
        return $this->hasMany(UserDigitalFootPrint::class, 'usereducationprofile_id');
    }

    public function user(){
        return $this->belongsTo(\App\Models\User::class,'user_id','id');
    }

    public function edelement(){
        return $this->belongsTo(EdElement::class, 'edelement_id','id');
    }
}