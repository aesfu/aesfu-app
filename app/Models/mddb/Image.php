<?php

namespace App\Models\mddb;

use Illuminate\Database\Eloquent\Model;

class Image extends Model
{
    public $timestamps = true;

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';

    protected $table = 'image';

    protected $fillable = [
        'name',
        'extension',
        'content',
        'size',
    ];

    protected $hidden = [

    ];
}
