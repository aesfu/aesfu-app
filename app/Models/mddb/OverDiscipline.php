<?php

namespace App\Models\mddb;

use Illuminate\Database\Eloquent\Model;

class OverDiscipline extends Model
{
    public $timestamps = false;

    protected $table = 'overdiscipline';

    protected $casts = [
        'period_at' => 'datetime',
    ];

    protected $fillable = [
        'branch_id',
        'user_id',
        'profile',
        'period_at',
    ];

    protected $hidden = [

    ];

    /**
     * Связь Многие к Одному с Branch
     */
    public function branch()
    {
        return $this->belongsTo(Branch::class, 'branch_id');
    }

    /**
     * Профиль по заданным параметрам
     */
    public static function profileWhere($branchId, $userId, $periodAt)
    {
        return self::whereBranchId($branchId)
            ->whereUserId($userId)
            ->where('period_at', '=', $periodAt->format('Y-m-01'))
            ->get()
            ->first();
    }

    /**
     * Все профили по заданным параметрам,
     * кроме группового
     */
    public static function profilesWhere($branchId, $usersId, $periodAt)
    {
        return self::whereBranchId($branchId)
            ->where('user_id', '<>', 0)
            ->whereIn('user_id', $usersId)
            ->where('period_at', '=', $periodAt->format('Y-m-01'))
            ->get();
    }
}
