<?php

namespace App\Models\mddb;

use Illuminate\Database\Eloquent\Model;

class EdElementType extends Model
{
    protected $table = 'edelementtype';
    public $timestamps = false;

    protected $fillable = [
        'name',
    ];

    protected $hidden = [];

}