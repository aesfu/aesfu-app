<?php

namespace App\Models\mddb;

use Illuminate\Database\Eloquent\Model;

class Connection extends Model
{
    protected $table = 'connection';
    protected $primaryKey = 'connection_id';
    public $timestamps = false;

    protected $fillable = [
        'edelement_from_id',
        'edelement_to_id',
        'about',
        'position',
        'external',
    ];

    protected $hidden = [];
    
}