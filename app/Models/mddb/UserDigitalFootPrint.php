<?php

namespace App\Models\mddb;

use Illuminate\Database\Eloquent\Model;

class UserDigitalFootPrint extends Model{

    const CREATED_AT = 'created_at';
    const UPDATED_AT = null;

    protected $table = 'userdigitalfootprint'; 
    public $timestamps = true;

    protected $casts = [
        'created_at' => 'datetime',

    ];

    protected $fillable = [
        'usereducationprofile_id',
        'page_from',
        'page_to',
        'page_component',
        'actiondfp',
        'parameters',
        'created_at',
    ];

    protected $hidden = [

    ];
}