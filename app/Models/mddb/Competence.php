<?php

namespace App\Models\mddb;

use Illuminate\Database\Eloquent\Model;
use App\Traits\Hierarchy;
class Competence extends Model
{
    use Hierarchy;
    protected $table = 'competence'; 
    public $timestamps = false;

    protected $fillable = [
        'name',
        'shortname',
        'code',
        'mapping',
        'level',
        'order',
        'parent_id',
    ];

    protected $hidden = [

    ];

    /**
     * Связь Многие к Многие с Discipline
     */
    public function disciplines()
    {
        return $this->belongsToMany(Discipline::class, 'disclinkcomp', 'competence_id', 'discipline_id');
    }
}
