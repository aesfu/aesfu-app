<?php

namespace App\Models\mddb;

use Illuminate\Database\Eloquent\Model;

class EducationCourse extends Model{
    protected $table = 'educationcourse'; 
    public $timestamps = false;

    protected $fillable = [
        'edelement_id',
        'name',
        'about',
    ];

    protected $hidden = [

    ];

    public function edelement(){
        return $this->belongsTo(EdElement::class,'edelement_id', 'id' );
    }
}