<?php

namespace App\Models\mddb;

use Illuminate\Database\Eloquent\Model;

class Speciality extends Model
{
    protected $table = 'speciality'; 
    public $timestamps = false;

    protected $fillable = [
        'code',
        'name',
        'shortname',
        'about',
    ];

    protected $hidden = [

    ];

    /**
     * Полное наименование направления подготовки
     */
    public function name()
    {
        return $this->code.' '.$this->name;
    }
}
