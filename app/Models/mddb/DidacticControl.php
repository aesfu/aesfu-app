<?php

namespace App\Models\mddb;

use Illuminate\Database\Eloquent\Model;

class DidacticControl extends Model
{
    protected $table = 'control';
    public $timestamps = false;

    protected $fillable = [
        'name',
    ];

    protected $hidden = [];

}