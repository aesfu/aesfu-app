<?php

namespace App\Models\mddb;

use Illuminate\Database\Eloquent\Model;

class Test extends Model
{
    const CREATED_AT = 'created_at';
    const UPDATED_AT = null;

    protected $table = 'test';
    public $timestamps = true;

    protected $casts = [
        'created_at' => 'datetime',
        'access_till' => 'datetime',
    ];

    protected $fillable = [
        'name',
        'portrait',
        'discipline_id',
        'timer',
        'owner',
        'option',
        'access_till',
        'created_at',
    ];

    protected $hidden = [

    ];

    /**
     * Связь Многие к Многие с Quest
     */
    public function quests()
    {
        return $this->belongsToMany(Quest::class, 'testlinkquest', 'test_id', 'quest_id')->withPivot('position');
    }

    /**
     * Связь Многие к Одному с Discipline
     */
    public function discipline()
    {
        return $this->belongsTo(Discipline::class, 'discipline_id');
    }

    /**
     * Связь Многие к Многие с Subgroup
     */
    public function subgroups()
    {
        return $this->belongsToMany(Subgroup::class, 'subgrouplinktest', 'test_id', 'subgroup_id');
    }

    /**
     * Связь Многие к Одному с User
     */
    public function userowner()
    {
        return $this->belongsTo(\App\Models\User::class, 'owner');
    }

    /**
     * Наименование формы тестирования
     */
    public function option()
    {
        return $this->option == 0 ? 'Анкетирование' : 'Тестирование';
    }
}
