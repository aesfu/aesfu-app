<?php

namespace App\Models\conf;

use Illuminate\Database\Eloquent\Model;

class Status extends Model
{
    protected $connection = 'conf';
    protected $table = 'status';

    protected $fillable = [
        'name',
        'type',
    ];

    protected $hidden = [

    ];

    /**
     * Связь Один к Многие с Article
     */
    public function articles()
    {
        return $this->hasMany(Article::class, 'status_id');
    }
}
