<?php

namespace App\Http\Requests\EducationCourse;

use Illuminate\Foundation\Http\FormRequest;

class UpdateRequest extends FormRequest{

    protected function prepareForValidation()
    {
        $this->merge([ 'id' => $this->route('id') ]);
    }

    public function rules()
    {
        return [
            'name' => 'max:150',
            'about' => 'max:1000',
        ];
    }

}
