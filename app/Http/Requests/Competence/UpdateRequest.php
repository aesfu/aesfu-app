<?php

namespace App\Http\Requests\Competence;

use Illuminate\Foundation\Http\FormRequest;

class UpdateRequest extends FormRequest{

    protected function prepareForValidation()
    {
        $this->merge([ 'id' => $this->route('id') ]);
    }


    public function rules()
    {
        return [
            'id' => 'exists:competence',
            'level' => 'numeric|min:0|max:10',
            'order' => 'numeric|min:1|max:1000',
            'name' => 'max:150',
            'shortname' => 'max:150',
            'parent_id' => 'exists:competence,id',
            'code' => 'max:150',
        ];
    }

}
