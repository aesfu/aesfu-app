<?php

namespace App\Http\Requests\Competence;

use Illuminate\Foundation\Http\FormRequest;

class CreateRequest extends FormRequest{
    public function rules()
    {
        return [
            'level' => 'required',
            'order' => 'required',
            'name' => 'max:500',
            'shortname' => 'max:150',
            'parent_id' => 'exists:competence,id|nullable',
            'code' => 'max:150',
        ];
    }

}
