<?php

namespace App\Http\Requests\QuestEdElement;

use Illuminate\Foundation\Http\FormRequest;

class CreateRequest extends FormRequest{
    public function rules()
    {
        return [
            'edelement_id' => 'exists:edelement,id|required',
            'quest_id' => 'exists:quest,id|required',
            'position' => 'numeric|nullable'
        ];
    }

}