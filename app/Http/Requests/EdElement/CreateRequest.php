<?php

namespace App\Http\Requests\EdElement;

use Illuminate\Foundation\Http\FormRequest;

class CreateRequest extends FormRequest{
    public function rules()
    {
        return [
            'level' => 'required',
            'order' => 'required',
            'name' => 'max:150',
            'shortname' => 'max:150',
            'parent_id' => 'exists:edelement,id|nullable',
            'edelementtype_id' => 'exists:edelementtype,id',
            'control_id' => 'exists:control,id|nullable',
            'discform_id' => 'exists:discform,id|nullable',
            'branch_id' => 'exists:branch,id|nullable',
            'discipline_id' => 'exists:discipline,id|nullable',
        ];
    }

}
