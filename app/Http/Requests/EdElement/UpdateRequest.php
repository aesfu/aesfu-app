<?php

namespace App\Http\Requests\EdElement;

use Illuminate\Foundation\Http\FormRequest;

class UpdateRequest extends FormRequest{

    protected function prepareForValidation()
    {
        $this->merge([ 'id' => $this->route('id') ]);
    }


    public function rules()
    {
        return [
            'id' => 'exists:edelement',
            'level' => 'numeric|min:0|max:10',
            'order' => 'numeric|min:1|max:1000',
            'name' => 'max:150',
            'shortname' => 'max:150',
            'parent_id' => 'exists:edelement,id',
            'edelementtype_id' => 'exists:edelementtype,id',
            'control_id' => 'exists:control,id|nullable',
            'discform_id' => 'exists:discform,id|nullable',
        ];
    }

}
