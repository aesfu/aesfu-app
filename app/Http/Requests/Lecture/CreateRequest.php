<?php

namespace App\Http\Requests\Lecture;

use Illuminate\Foundation\Http\FormRequest;

class CreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        return [
            'user_id' => 'required|exists:User,id',
            'order' => 'nullable|numeric|between:0,100',
            'name' => 'required|string|between:3,255',
            'discipline_id' => 'nullable|exists:Discipline,id',
            'document' => 'required|file|mimetypes:application/json',
        ];
    }
}
