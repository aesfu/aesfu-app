<?php

namespace App\Http\Requests\Lecture;

use Illuminate\Validation\Rule;
use Illuminate\Foundation\Http\FormRequest;

class ModelRequest extends FormRequest
{
    /**
     * Allowed relationships, that can be eager loaded.
     */
    protected $withAllowed = ['user', 'document', 'discipline'];

    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Prepare the data for validation.
     *
     * @return void
     */
    protected function prepareForValidation()
    {
        $this->merge([
            'with' => explode(',', $this->input('with')),
        ]);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        return [
            'with' => 'nullable',
            'with.*' => Rule::in($this->withAllowed),
            'perPage' => 'numeric|min:1|max:1000',
            'pageCount' => 'numeric|min:1',
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array<string, string>
     */
    public function messages(): array
    {
        $withAllowedStr = join(', ', $this->withAllowed);

        return [
            'with.*' => "Значение поля отсутствует в списке разрешённых: $withAllowedStr",
        ];
    }
}
