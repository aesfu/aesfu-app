<?php

namespace App\Http\Requests\CompetenceEdElement;

use Illuminate\Foundation\Http\FormRequest;

class CreateRequest extends FormRequest{
    public function rules()
    {
        return [
            'edelement_id' => 'exists:edelement,id|required',
            'competence_id' => 'exists:competence,id|required',
        ];
    }

}