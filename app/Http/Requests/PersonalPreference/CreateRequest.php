<?php

namespace App\Http\Requests\PersonalPreference;

use Illuminate\Foundation\Http\FormRequest;

class CreateRequest extends FormRequest{
    public function rules()
    {
        return [
            'answer_id' => 'required|exists:answer,id',
            'edelement_id' => 'exists:edelement,id',
            'competence_id' => 'exists:competence,id',
            'task_id' => 'exists:task,id',
            'connectionType' => 'numeric',
            'compName' => 'max:500',
            'compShort' => 'max:150',
            'compCode' => 'max:150',
            'edelemName' => 'max:500',
            'edelemShort' => 'max:150',
        ];
    }

}
