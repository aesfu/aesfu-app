<?php

namespace App\Http\Requests\PersonalPreference;

use Illuminate\Foundation\Http\FormRequest;

class UpdateRequest extends FormRequest{

    protected function prepareForValidation()
    {
        $this->merge([ 'id' => $this->route('id') ]);
    }


    public function rules()
    {
        return [
            'id' => 'exists:personalpreference',
        ];
    }

}
