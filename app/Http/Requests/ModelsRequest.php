<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ModelsRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Convert to boolean
     *
     * @param $booleable
     * @return boolean
     */
    private function toBoolean($booleable)
    {
        return filter_var($booleable, FILTER_VALIDATE_BOOLEAN, FILTER_NULL_ON_FAILURE);
    }

    /**
     * Prepare the data for validation.
     *
     * @return void
     */
    protected function prepareForValidation()
    {
        if ($this->has('with')) {
            $this->merge([ 'with' => explode(',', $this->with) ]);
        }
        if ($this->has('edelement')){
            $this->merge([
                'edelement' => $this->toBoolean($this->edelement),
            ]);
        }
        if ($this->has('summary')){
            $this->merge([
                'summary' => $this->toBoolean($this->summary),
            ]);
        }
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'root' => 'boolean',
            'parent_id' => 'nullable|numeric',
            'discipline_id' => 'nullable|numeric',
            'level' => 'numeric|min:0|max:10',
            'with' => 'nullable|array',
            'summary' => 'boolean',
            'perPage' => 'numeric|min:1|max:1000',
            'pageCount' => 'numeric|min:1',
            'trashed' => 'boolean',
            'test_id' => 'exists:test,id|nullable|numeric',
            'speciality_id' => 'exists:speciality,id|nullable|numeric',
            'basket_id' => 'exists:basket,id|nullable|numeric',
            'edelement' => 'boolean',
        ];
    }
}
