<?php

namespace App\Http\Requests\Task;

use Illuminate\Foundation\Http\FormRequest;

class UpdateRequest extends FormRequest{
    protected function prepareForValidation()
    {
        $this->merge([ 'connection_id' => $this->route('id') ]);
    }
    
    public function rules()
    {
        return [
            'name' => 'max:100',
        ];
    }

}