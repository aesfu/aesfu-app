<?php

namespace App\Http\Requests\Auth;

use Illuminate\Foundation\Http\FormRequest;

class InfoUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        return [
            'surname' => 'string|max:50',
            'name' => 'string|max:50',
            'patronym' => 'string|max:50',
            'statusId' => 'integer|exists:userstatus,id',
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array<string, string>
     */
    public function messages(): array
    {
        return [
            'max' => 'Максимальная допустимая длина :max символов',
            'string' => 'Значение должно быть строковым',
            'integer' => 'Значение должно быть целочисленным',
        ];
    }
}
