<?php

namespace App\Http\Requests\Auth;

use Illuminate\Support\Facades\Hash;
use Illuminate\Foundation\Http\FormRequest;

class PasswordUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'current' => function ($attribute, $value, $fail) {
                if (!Hash::check($value, \Auth::user()->password)) {
                    $fail('Старый пароль не соответствует текущему');
                }
            },
            'password' => 'required|string|min:6|confirmed|different:current',
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'required' => 'Поле обязательно к заполнению',
            'min' => 'Минимальная длина :min символов',
            'password.confirmed' => 'Не верное подтверждение пароля',
            'password.different' => 'Новый пароль не может совпадать с предыдущим',
        ];
    }
}
