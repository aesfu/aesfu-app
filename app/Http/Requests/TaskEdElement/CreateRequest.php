<?php

namespace App\Http\Requests\TaskEdElement;

use Illuminate\Foundation\Http\FormRequest;

class CreateRequest extends FormRequest{
    public function rules()
    {
        return [
            'edelement_id' => 'exists:edelement,id|required',
            'task_id' => 'exists:task,id|required',
        ];
    }

}