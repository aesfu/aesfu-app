<?php

namespace App\Http\Requests\UserEducationCourse;

use Illuminate\Foundation\Http\FormRequest;

class UpdateRequest extends FormRequest{

    protected function prepareForValidation()
    {
        $this->merge([ 'id' => $this->route('id') ]);
    }

    public function rules()
    {
        return [
            'current_edelement_id' => 'exists:edelement,id',
            'score' => 'numeric',
            'finished' => 'numeric',
        ];
    }

}
