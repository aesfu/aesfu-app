<?php

namespace App\Http\Requests\Connection;

use Illuminate\Foundation\Http\FormRequest;

class CreateRequest extends FormRequest{
    public function rules()
    {
        return [
            'edelement_from_id' => 'exists:edelement,id|required',
            'edelement_to_id' => 'exists:edelement,id|required',
            'about' => 'max:100',
            'position' => 'numeric|nullable',
            'external' => 'numeric|nullable',
        ];
    }

}