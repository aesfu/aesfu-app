<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\ValidationException;

use App\Models\User;
use App\Models\UserToken;
use App\Http\Requests\Auth\SanctumPostRequest;

class SanctumController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Система авторизации Sanctum
    |--------------------------------------------------------------------------
    */

    /**
     * Данные о пользователе через токен
     *
     * GET /api/token/auth
     */
    public function auth(Request $request)
    {
        if (Auth::check()) {
            $user = Auth::user();
        } else if ($request->filled('token')) {
            $token = UserToken::findToken($request->token);

            if ($token) {
                $user = $token->tokenable;
            }
        } else if ($request->filled('login') && $request->filled('password')) {
            $user = $this->authenticate($request);
        }

        if (!isset($user)) {
            return $this->toJson(['message' => 'Unauthenticated.'], 401);
        }

        $user->load(['userstatus', 'permissions']);

        if ($user->branch) {
            $user->branch
                ->setHidden(['speciality_id'])
                ->speciality;
        }

        $user->permissions = $user->permissions->map(function ($elem) {
            return $elem->setHidden([
                'guard_name',
                'created_at',
                'updated_at',
                'pivot',
            ]);
        });

        return $this->toJson($user);
    }

    /**
     * Авторизация и получение токена доступа
     *
     * POST /api/token/login
     */
    public function login(SanctumPostRequest $request)
    {
        $user = $this->authenticate($request);

        $name = $request->filled('name') ? $request->name : 'aesu';

        $user->_token = $user->createToken($name)->plainTextToken;

        return $this->toJson($user);
    }

    /**
     * Отозвать все собственные токены
     *
     * DELETE /api/token/revoke
     */
    public function revoke(Request $request)
    {
        $tokens = Auth::user()->tokens();

        if ($request->filled('name')) {
            $tokens->whereName($request->name);
        }

        return $this->toJson([
            'result' => true,
            'revoked' => $tokens->delete(),
        ]);
    }

    /**
     * Попытка авторизации
     */
    protected function authenticate($request)
    {
        $user = User::where('login', $request->login)->first();

        if (!$user || !Hash::check($request->password, $user->password)) {
            throw ValidationException::withMessages([
                'login' => ['Пользователь с таким логином и паролем не найден'],
            ]);
        }

        return $user;
    }

    /**
     * Тестовый метод отладки
     *
     * GET /api/token/check
     */
    public function check()
    {
        return $this->toJson(['Sanctum' => 'Hello, World!']);
    }
}
