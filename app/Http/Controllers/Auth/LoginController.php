<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    protected $redirectTo = '/';

    protected $username = 'login';

    /**
     * Количество попыток на авторизацию
     */
    protected $maxAttempts = 3;

    /**
     * Интервал между повторной авторизацией
     */
    protected $decayMinutes = 1;

    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    /**
     * Наименование поля логина для авторизации
     */
    public function username()
    {
        return $this->username;
    }

    /**
     * Отображение формы авторизации
     */
    public function showLoginForm()
    {
        return view('app');
    }

    /**
     * Проверка правильности ввода данных о пользователе
     */
    protected function validateLogin(Request $request)
    {
        if ($request->has('timezone')) $request->session()->put('timezone', $request->timezone);

        $this->validate($request, [
            $this->username() => 'required|string',
            'password' => 'required|string',
        ], [
            $this->username() . '.required' => 'Логин обязателен к заполнению',
            'password.required' => 'Пароль обязателен к заполнению',
        ]);
    }

    /**
     * Отправка ответа после авторизации пользователя
     */
    protected function sendLoginResponse(Request $request)
    {
        $request->session()->regenerate();

        $this->clearLoginAttempts($request);

        return $this->authenticated($request, $this->guard()->user()) ?:
            response()->json([
                'result' => true,
                'intended' => redirect()->intended()->getTargetUrl(),
            ]);
    }
}
