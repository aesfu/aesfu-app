<?php

namespace App\Http\Controllers;

use App\Models;
use App\Models\Logs;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Pagination\LengthAwarePaginator;

/**
 * !!! ПЕРЕНЕСТИ ФУНКЦИОНАЛ НА REST API !!!
 */

class ConsoleController extends Controller
{
    public function main(Request $request)
    {
        if ((int) date('m') < 9) {
            return view('console.main');
        }

        $year = (int) date('Y');

        $articles = \App\Models\conf\Article::whereYear('created_at', $year)->with('members')->get();

        $members = $articles->reduce(function ($red, $elem) {
            $red->push(...$elem->members);
            return $red;
        }, collect())
            ->unique('id');

        return view('console.main', [
            'year' => $year,
            'articles' => $articles,
            'members' => $members,
        ]);
    }

    // >> СПЕЦИАЛЬНОСТИ <<

    public function Speciality(Request $request)
    {
        $views = [];

        $specs = Models\mddb\Speciality::orderBy('code')->get();

        foreach ($specs as $spec) {
            $views[] = \View::make('console.speciality.parts.speciality', [
                'spec' => $spec,
            ]);
        }

        return view('console.speciality.specialities', [
            'views' => $views,
        ]);
    }

    // >> ФАКУЛЬТАТИВНЫЕ ГРУППЫ <<

    public function Branch(Request $request)
    {
        return view('console.branch.branches');
    }

    public function BranchNext(Request $request) // [ASYNC]
    {
        $branches = Models\mddb\Branch::orderBy('name')->get();
        $branches = $this->branchFilter($branches, $request->sort);

        $page = $request->page;
        $branches = $this->paginateModels($branches, $page, 15);

        $views = $this->branchViews($branches);

        return response()->json(['views' => $views, 'last' => $page >= $branches->lastPage()]);
    }

    public function BranchSearch(Request $request) // [ASYNC]
    {
        if (!$request->has('param') || $request->param == '') {
            return response()->json([]);
        }

        $params = explode(' ', $request->param);
        $branches = Models\mddb\Branch::where('name', 'like', '%' . reset($params) . '%');

        foreach ($params as $param) {
            $branches->where('name', 'like', '%' . $param . '%');
        }

        $branches = $this->branchFilter($branches->get(), $request->sort);
        $views = $this->branchViews($branches);

        return response()->json(['views' => $views]);
    }

    private function branchViews($branches)
    {
        $views = [];

        foreach ($branches as $branch) {
            $views[] = \View::make('console.branch.parts.branch', [
                'branch' => $branch,
            ])->render();
        }

        return $views;
    }

    private function branchFilter($branches, $sort)
    {
        $sort = $sort ?? '-';

        switch ($sort) {
            case '-':
                $branches = $branches->sortBy('name');
                break;
            case 'za':
                $branches = $branches->sortByDesc('name');
                break;
        }

        return $branches;
    }

    // >> СПЕЦГРУППЫ <<

    public function Subgroup(Request $request)
    {
        return view('console.subgroup.subgroups');
    }

    public function SubgroupNext(Request $request) // [ASYNC]
    {
        $groups = Models\mddb\Subgroup::orderBy('name')->get();
        $groups = $this->subgroupFilter($groups, $request->sort);

        $page = $request->page;
        $groups = $this->paginateModels($groups, $page, 15);

        $views = $this->subgroupViews($groups);

        return response()->json(['views' => $views, 'last' => $page >= $groups->lastPage()]);
    }

    public function SubgroupSearch(Request $request) // [ASYNC]
    {
        if (!$request->has('param') || $request->param == '') {
            return response()->json([]);
        }

        $params = explode(' ', $request->param);
        $groups = Models\mddb\Subgroup::where('name', 'like', '%' . reset($params) . '%');

        foreach ($params as $param) {
            $groups->where('name', 'like', '%' . $param . '%');
        }

        $groups = $this->subgroupFilter($groups->get(), $request->sort);
        $views = $this->subgroupViews($groups);

        return response()->json(['views' => $views]);
    }

    private function subgroupViews($groups)
    {
        $views = [];

        foreach ($groups as $group) {
            $views[] = \View::make('console.subgroup.parts.subgroup', [
                'group' => $group,
            ])->render();
        }

        return $views;
    }

    private function subgroupFilter($groups, $sort)
    {
        $sort = $sort ?? '-';

        switch ($sort) {
            case '-':
                $groups = $groups->sortBy('name');
                break;
            case 'za':
                $groups = $groups->sortByDesc('name');
                break;
        }

        return $groups;
    }

    // >> ДИСЦИПЛИНЫ <<

    public function Discipline(Request $request)
    {
        return view('console.discipline.disciplines');
    }

    public function DisciplineNext(Request $request) // [ASYNC]
    {
        $discs = Models\mddb\Discipline::orderBy('name')->get();
        $discs = $this->disciplineFilter($discs, $request->sort);

        $page = $request->page;
        $discs = $this->paginateModels($discs, $page, 15);

        $views = $this->disciplineViews($discs);

        return response()->json(['views' => $views, 'last' => $page >= $discs->lastPage()]);
    }

    public function DisciplineSearch(Request $request) // [ASYNC]
    {
        if (!$request->has('param') || $request->param == '') {
            return response()->json([]);
        }

        $params = explode(' ', $request->param);
        $discs = Models\mddb\Discipline::where('name', 'like', '%' . reset($params) . '%');

        foreach ($params as $param) {
            $discs->where('name', 'like', '%' . $param . '%');
        }

        $discs = $this->disciplineFilter($discs->get(), $request->sort);
        $views = $this->disciplineViews($discs);

        return response()->json(['views' => $views]);
    }

    private function disciplineViews($discs)
    {
        $views = [];

        foreach ($discs as $disc) {
            $views[] = \View::make('console.discipline.parts.discipline', [
                'disc' => $disc,
            ])->render();
        }

        return $views;
    }

    private function disciplineFilter($discs, $sort)
    {
        $sort = $sort ?? '-';

        switch ($sort) {
            case '-':
                $discs = $discs->sortBy('name');
                break;
            case 'za':
                $discs = $discs->sortByDesc('name');
                break;
        }

        return $discs;
    }

    // >> КОМПЕТЕНЦИИ <<

    public function Competence(Request $request)
    {
        return view('console.competence.competences');
    }

    public function CompetenceNext(Request $request) // [ASYNC]
    {
        $competences = Models\mddb\Competence::orderBy('name')->get();

        $page = $request->page;
        $competences = $this->paginateModels($competences, $page);

        $views = $this->competenceViews($competences);

        return response()->json(['views' => $views, 'last' => $page >= $competences->lastPage()]);
    }

    private function competenceViews($competences)
    {
        $views = [];

        foreach ($competences as $competence) {
            $views[] = \View::make('console.competence.parts.competence', [
                'competence' => $competence,
            ])->render();
        }

        return $views;
    }

    // >> ТЕСТЫ <<

    public function Test(Request $request)
    {
        return view('console.test.tests');
    }

    public function TestNext(Request $request) // [ASYNC]
    {
        $tests = Models\mddb\Test::all();
        $tests = $this->testFilter($tests, $request->sort);

        $page = $request->page;
        $tests = $this->paginateModels($tests, $page);

        $views = $this->testViews($tests);

        return response()->json(['views' => $views, 'last' => $page >= $tests->lastPage()]);
    }

    public function TestSearch(Request $request) // [ASYNC]
    {
        if (!$request->has('param') || $request->param == '') {
            return response()->json([]);
        }

        $params = explode(' ', $request->param);
        $quests = Models\mddb\Test::where('name', 'like', '%' . reset($params) . '%');

        foreach ($params as $param) {
            $quests->where('name', 'like', '%' . $param . '%');
        }

        $tests = $this->testFilter($quests->get(), $request->sort);
        $views = $this->testViews($tests);

        return response()->json(['views' => $views]);
    }

    private function testViews($tests)
    {
        $views = [];

        foreach ($tests as $test) {
            $views[] = \View::make('console.test.parts.test', [
                'test' => $test,
                'access_till' => $this->reformatDate($test->access_till, 'd.m.Y H:i:s'),
                'created_at' => $this->reformatDate($test->created_at, 'd.m.Y H:i:s'),
            ])->render();
        }

        return $views;
    }

    private function testFilter($tests, $sort)
    {
        $sort = $sort ?? '-';

        switch ($sort) {
            case '-':
                $tests = $tests->sortBy('name');
                break;
            case 'dn':
                $tests = $tests->sortByDesc('created_at');
                break;
            case 'do':
                $tests = $tests->sortBy('created_at');
                break;
        }

        return $tests;
    }

    // >> ВОПРОСЫ <<

    public function Quest(Request $request)
    {
        return view('console.quest.quests');
    }

    public function QuestNext(Request $request) // [ASYNC]
    {
        $quests = Models\mddb\Quest::all();
        $quests = $this->questFilter($quests, $request->sort);

        $page = $request->page;
        $quests = $this->paginateModels($quests, $page, 100);

        $views = $this->questViews($quests);

        return response()->json(['views' => $views, 'last' => $page >= $quests->lastPage()]);
    }

    public function QuestSearch(Request $request) // [ASYNC]
    {
        if (!$request->has('param') || $request->param == '') {
            return response()->json([]);
        }

        $params = explode(' ', $request->param);
        $quests = Models\mddb\Quest::where('name', 'like', '%' . reset($params) . '%');

        foreach ($params as $param) {
            $quests->where('name', 'like', '%' . $param . '%');
        }

        $quests = $this->questFilter($quests->get(), $request->sort);
        $views = $this->questViews($quests);

        return response()->json(['views' => $views]);
    }

    private function questViews($quests)
    {
        $views = [];

        foreach ($quests as $quest) {
            $views[] = \View::make('console.quest.parts.quest', [
                'quest' => $quest,
            ])->render();
        }

        return $views;
    }

    private function questFilter($quests, $sort)
    {
        $sort = $sort ?? '-';

        switch ($sort) {
            case '-':
                $quests = $quests->sortBy('name');
                break;
            case 'za':
                $quests = $quests->sortByDesc('name');
                break;
        }

        return $quests;
    }

    // >> ПОЛЬЗОВАТЕЛИ <<

    public function User(Request $request)
    {
        return view('console.user.users');
    }

    public function UserNext(Request $request) // [ASYNC]
    {
        $users = Models\User::all();
        $users = $this->userFilter($users, $request->sort);

        $page = $request->page;
        $users = $this->paginateModels($users, $page);

        $views = $this->userViews($users);

        return response()->json(['views' => $views, 'last' => $page >= $users->lastPage()]);
    }

    public function UserSearch(Request $request) // [ASYNC]
    {
        if (!$request->has('param') || $request->param == '') {
            return response()->json([]);
        }

        $params = explode(' ', $request->param);
        $users = Models\User::whereRaw('concat(user.surname, " ", user.name, " ", user.patronym) like ?', '%' . reset($params) . '%');

        foreach ($params as $param) {
            $users->whereRaw('concat(user.surname, " ", user.name, " ", user.patronym) like ?', '%' . $param . '%');
        }

        $users = $this->userFilter($users->get(), $request->sort);
        $views = $this->userViews($users);

        return response()->json(['views' => $views]);
    }

    private function userViews($users)
    {
        $views = [];

        foreach ($users as $user) {
            $views[] = \View::make('console.user.parts.user', [
                'user' => $user,
            ])->render();
        }

        return $views;
    }

    private function userFilter($users, $sort)
    {
        $sort = $sort ?? '-';

        switch ($sort) {
            case 'nm':
                $users = $users->sortBy('surname');
                break;
            case '-':
                $users = $users->sortByDesc('created_at');
                break;
            case 'do':
                $users = $users->sortBy('created_at');
                break;
        }

        return $users;
    }

    // >> ПРОЙДЕННЫЕ <<

    public function Passed(Request $request)
    {
        return view('console.passed.passed');
    }

    public function PassedNext(Request $request) // [ASYNC]
    {
        $baskets = Models\mddb\Basket::orderBy('passed_at', 'desc')->get();
        $baskets = $this->passedFilter($baskets, $request->sort);

        $page = $request->page;
        $baskets = $this->paginateModels($baskets, $page);

        $views = $this->passedViews($baskets);

        return response()->json(['views' => $views, 'last' => $page >= $baskets->lastPage()]);
    }

    public function PassedSearch(Request $request) // [ASYNC]
    {
        if (!$request->has('param') || $request->param == '') {
            return response()->json([]);
        }

        $params = explode(' ', $request->param);

        $baskets = \DB::table('basket')
            ->select(\DB::raw('basket.id, concat(test.name, " ", discipline.name, " ", user.surname, " ", user.name, " ", user.patronym) as name'))
            ->join('test', 'test.id', '=', 'basket.test_id')
            ->join('discipline', 'discipline.id', '=', 'test.discipline_id')
            ->join('user', 'user.id', '=', 'basket.user_id');

        foreach ($params as $param) {
            $baskets->whereRaw('concat(test.name, " ", discipline.name, " ", user.surname, " ", user.name, " ", user.patronym) like ?', '%' . $param . '%');
        }

        $baskets = $baskets->get();

        // debug($baskets);

        $basket = $baskets->map(function ($val) {
            return $val->id; })->unique()->toArray();

        $baskets = Models\mddb\Basket::whereIn('id', $basket)->get();

        $views = $this->passedViews($baskets);

        return response()->json(['views' => $views]);
    }

    public function PassedDownload(Request $request, $id) // [ASYNC]
    {
        $basket = Models\mddb\Basket::find($id);

        if (!$basket)
            return $this->toJson(['message' => 'Результаты прохождения тесте не найдены'], 422);

        $test = $basket->test;

        if (!$test)
            return $this->toJson(['message' => 'Тест для результата прохождения не найден'], 422);

        $results = Models\mddb\Result::whereBasketId($basket->id)->get();

        if ($results->count() === 0)
            return $this->toJson(['message' => 'Результаты для данного прохождения не найдены'], 422);

        $user = $basket->user;

        if (!$user)
            return $this->toJson(['message' => 'Пользователь, который проходил тестирование, не найден'], 422);

        $quests = $test->quests()
            ->with(['questtype', 'answers'])
            ->orderBy('position')
            ->get();

        $name = $user->surname . mb_substr($user->name, 0, 1) . mb_substr($user->patronym, 0, 1) . ' _';
        $name .= $test->name . ' ';
        $name .= '[' . $this->reformatDate($basket->passed_at, 'd.m.Y') . '].csv';

        $content = 'Пройдено;' . $this->reformatDate($basket->passed_at, 'd.m.Y H:i:s') . "\r\n";
        $content .= 'Имя полльзователя;' . $user->name() . "\r\n";
        $content .= 'Напрвление подготовки;' . ($user->branch ? $user->branch->speciality->name() : '-') . "\r\n";
        $content .= 'Группа;' . ($user->branch ? $user->branch->name : '-') . "\r\n";
        $content .= 'Дисциплина;' . ($basket->test->discipline ? $basket->test->discipline->name : '-') . "\r\n";
        $content .= 'Тест;' . $basket->test->name . "\r\n\r\n";
        $content .= "Вопрос;Позиция\r\n";

        $quests->each(function ($elem) use ($results, &$content) {
            $resultsForQuest = $results->where('quest_id', $elem->id);

            $type = $elem->questtype->type;

            if (in_array($type, [0, 1, 2])) {
                $answers = $elem->answers
                    ->sortBy('position')
                    ->map(function ($_elem) use ($resultsForQuest) {
                        if ($resultsForQuest->contains('answer_id', $_elem->id))
                            return 1;
                        else
                            return 0;
                    });

                if ($answers->count() < 6) {
                    for ($q = $answers->count(); $q < 6; $q++)
                        $answers->push(0);
                }
            } else if ($type === 3) {
                $answers = collect([$resultsForQuest->first()->value]);
            } else if ($type === 4) {
                $answers = $elem->answers
                    ->sortBy('position')
                    ->map(function ($_elem) use ($resultsForQuest) {
                        $resultForQuest = $resultsForQuest->where('answer_id', $_elem->id)->first();

                        return $resultForQuest->value;
                    });
            }

            // if (!$answers) return $reducer;

            $content .= $elem->name . ';' . $elem->pivot->position;

            foreach ($answers as $answer)
                $content .= ';' . $answer;

            $content .= "\r\n";
        });

        $content = mb_convert_encoding($content, 'Windows-1251');

        $response = \Response::make($content, 200);
        $response->header('Content-Disposition', "filename={$name}");
        $response->header('Content-Type', 'text/csv');

        return $response;
    }

    private function passedFilter($baskets, $sort)
    {
        $sort = $sort ?? '-';

        switch ($sort) {
            case 'bl':
                $baskets = $baskets->filter(function ($val) {
                    return $val->test->option == 0; });
                break;
            case 'qz':
                $baskets = $baskets->filter(function ($val) {
                    return $val->test->option == 1; });
                break;
        }

        return $baskets;
    }

    private function passedViews($baskets)
    {
        $views = [];

        foreach ($baskets as $basket) {
            $views[] = \View::make('console.passed.parts.passed', [
                'basket' => $basket,
                'test' => $basket->test ?? null,
                'passed_at' => $this->reformatDate($basket->passed_at, 'd.m.Y H:i:s'),
            ])->render();
        }

        return $views;
    }

    // >> СТАТЬИ НА КОНФЕРЕНЦИЮ <<

    public function Conference(Request $request)
    {
        return view('console.conference.main', [
            'statuses' => \App\Models\conf\Status::all(),
        ]);
    }

    // >> РАЗРАБОТЧИКАМ <<

    public function Develop(Request $request)
    {
        $filename = "laravel.log";

        if (!Storage::disk('logs')->has($filename))
            Storage::disk('logs')->put($filename, '');

        $laravel = Models\log\Laravel::all();
        $react = Models\log\React::all();
        $develops = Models\User::permission('develop')->orderBy('surname')->get();

        return view('console.develop.develop', [
            'laravel' => $laravel,
            'react' => $react,
            'develops' => $develops,
        ]);
    }

    public function DownloadLocal(Request $request)
    {
        if (!$request->has('name') || !$request->has('mime'))
            return $this->toJson(['message' => 'Не все параметры заполнены'], 422);

        $name = $request->name;

        if (!Storage::disk('public')->exists($name))
            return $this->toJson(['message' => 'Документ не найден'], 422);

        $file = Storage::disk('public')->get($name);

        $response = \Response::make($file, 200);
        $response->header('Content-Disposition', "filename={$name}");
        $response->header('Content-Type', "{$request->mime}");

        return $response;
    }

    // >> СОБСТВЕННЫЕ МЕТОДЫ <<

    protected function paginateModels($models, $page, $perPage = 50)
    {
        return new LengthAwarePaginator(
            $models->forPage($page, $perPage),
            $models->count(),
            $perPage,
            $page,
            []
        );
    }

    protected function serverError($msg)
    {
        return [
            'header' => '500 Internal Server Error',
            'message' => 'ERROR: ' . $msg,
        ];
    }
}
