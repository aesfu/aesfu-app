<?php

namespace App\Http\Controllers\Other;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Cache;

class CacheController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Контроллер для чтения кеша
    |--------------------------------------------------------------------------
    */

    /**
     * Получение статистики запросов скрипта flm-builder.py
     *
     * GET /api/cache/flm-builder
     */
    public function flmBuilderStat()
    {
        $obj = Cache::get('flm-builder-stat', [
            'total' => 0,
            'success' => 0,
            'errors' => 0,
            'last_exec_at' => now(),
        ]);

        $lastExecAt = $obj['last_exec_at']
            ->setTimezone('Asia/Krasnoyarsk')
            ->format('d.m.Y H:i:s (P)');

        return response(
            '<div>' .
            '<h3 style="margin-bottom:.5rem">Запуски:</h3>' .
            '<ul style="margin:0;margin-bottom:.5rem">' .
            '<li>Успехов: <i>' . $obj['success'] . '</i></li>' .
            '<li>Ошибок: <i>' . $obj['errors'] . '</i></li>' .
            '<li>Всего: <i>' . $obj['total'] . '</i></li>' .
            '</ul>' .
            '</div>' .
            '<div>' .
            'Последний запуск: <i>' . $lastExecAt . '</i>' .
            '</div>'
        );
    }
}
