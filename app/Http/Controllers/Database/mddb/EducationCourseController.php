<?php

namespace App\Http\Controllers\Database\mddb;

use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\mddb\EducationCourse;
use App\Http\Requests\EducationCourse as Requests;
use App\Http\Requests\ModelsRequest;

class EducationCourseController extends Controller
{
    public function __construct()
    {
        // $this->middleware('')->only([ '' ]);
    }

    // Получение информации об образовательном курсе пользователя
    // [ GET /api/course/{id} ]
    public function educationcourse($id)
    {
        $course = EducationCourse::find($id);

        if (!$course)
            return $this->toJson(['message' => 'Не существует'], 200);

        return $course->toJson();
    }

    // Создание образовательного курса пользователя
    // [ POST /api/course/create ]
    public function create(Requests\CreateRequest $request)
    {
        $model = EducationCourse::create($request->validated());

        return $this->toJson($model->fresh());
    }

    // Изменение образовательного курса пользователя
    // [ PUT /api/course/{id} ]
    public function update(Requests\UpdateRequest $request, $id)
    {
        EducationCourse::find($id)
            ->update($request->validated());

        return $this->toJson(true);
    }

    // Удаление образовательного курса пользователя
    // [ DELETE /api/course/{id} ]
    public function delete($id)
    {

        Validator::validate(['id' => $id], ['id' => 'exists:educationcourse']);

        EducationCourse::find($id)->delete();

        return $this->toJson(true);
    }

    // Постраничный список всех образовательных курсов
    // [ POST /api/courses ]
    public function models(ModelsRequest $request)
    {
        $models = EducationCourse::with('edelement');

        $models = $this->paginator(
            $models,
            $request->perPage,
            $request->pageCount,
        );

        return $this->toJson($models);
    }
}
