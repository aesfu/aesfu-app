<?php

namespace App\Http\Controllers\Database\mddb;

use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\ModelsRequest;
use App\Http\Requests\Task as Requests;

use App\Models\mddb\Task;

class TaskController extends Controller
{
    public function __construct()
    {
        // $this->middleware('')->only([ '' ]);
    }
    /**
     * POST /api/task
     */
    public function create(Requests\CreateRequest $request)
    {
        $model = Task::create($request->validated());

        return $this->toJson($model->fresh());
    }

    /**
     * DELETE /api/task/{id}
     */
    public function delete($id)
    {
        Validator::validate(['id' => $id], ['id' => 'exists:task']);

        Task::find($id)->delete();

        return $this->toJson(true);
    }

    /**
     *  PUT /api/task/{id}
     */
    public function update(Requests\UpdateRequest $request, $id)
    {
        Task::find($id)->update($request->validated());

        return $this->toJson(true);
    }
    /**
     *  POST /api/tasks
     */
    public function models(ModelsRequest $request)
    {
        $models = Task::query()
            ->when($request->name, function ($query, $value) {
                $words = explode(' ', $value);

                foreach ($words as $word) {
                    $query->where('name', 'like', '%' . $word . '%');
                }
            })
            ->orderBy('id');

        $models = $this->paginator(
            $models,
            $request->perPage,
            $request->pageCount,
        );

        return $this->toJson($models);
    }
}