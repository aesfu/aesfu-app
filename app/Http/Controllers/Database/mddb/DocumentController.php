<?php

namespace App\Http\Controllers\Database\mddb;

use Illuminate\Support\Str;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;

use App\Models\mddb\Document;

class DocumentController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Получение локальных файлов и документов из базы данных
    |--------------------------------------------------------------------------
    */

    /**
     * Получение данных о документе
     *
     * GET /api/document/{ulid}
     */
    public function model(string $ulid)
    {
        $document = Document::find($ulid);

        if (!$document) {
            return $this->toJson(
                ['message' => 'Документ не найден или не существует.'],
                422,
            );
        }

        $document->exists = Storage::disk('public')->exists($document->path);

        return $this->toJson($document);
    }

    /**
     * Получение данных о документе
     *
     * GET /api/document/{ulid}
     */
    public function delete(string $ulid)
    {
        $document = Document::find($ulid);

        if (!$document) {
            return $this->toJson(
                ['message' => 'Документ не найден или не существует.'],
                422,
            );
        }

        Storage::disk('public')->delete($document->path);

        $document->delete();

        return $this->toJson(['status' => true]);
    }

    /**
     * Получение файла
     *
     * GET /document?k=#
     */
    public function load(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'k' => 'required|ulid',
        ], [
            'k.required' => 'Поле :attribute обязательно для указания ulid ключа документа.',
        ]);

        if ($validator->fails()) {
            if ($request->expectsJson()) {
                $validator->validate();
            } else {
                return response()
                    ->view('errors.Error', ['errors' => $validator->errors()], 404);
            }
        }

        $document = Document::find($request->k);

        if (!$document || Storage::disk('public')->missing($document->path)) {
            $data = ['message' => 'Документ не найден или не существует.'];

            if ($request->expectsJson()) {
                return $this->toJson($data, 422);
            } else {
                return response()->view('errors.Error', $data, 404);
            }
        }

        return Storage::response($document->path, Str::slug($document->name));
    }

    /**
     * Получение файла из локальной папки storage
     *
     * GET /document/local/*
     */
    public function localLoad($path)
    {
        if (Storage::disk('public')->missing($path)) {
            return response()->view(
                'errors.Error',
                ['error' => 'Not Found', 'message' => 'Документ не найден или не существует.'],
                404
            );
        }

        return Storage::response($path);
    }
}
