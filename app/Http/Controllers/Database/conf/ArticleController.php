<?php

namespace App\Http\Controllers\Database\conf;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;

use App\Models\conf\Member;
use App\Models\conf\Status;
use App\Models\conf\Article;

class ArticleController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Статья на конференцию
    |--------------------------------------------------------------------------
    */

    public function __construct()
    {
        // $this->middleware('')->only([ '' ]);
    }

    /**
     * Получение статьи
     *
     * GET /api/conf/article/{id}
     */
    public function article($id)
    {
        $article = Article::find($id);

        if (!$article)
            return $this->toJson(['message' => 'Научная статья не найдена'], 422);

        $article->load([
            'status',
            'section',
            'members',
            'documents',
        ]);

        return $this->toJson($article);
    }

    /**
     * Обновление статуса статьи
     *
     * PUT /api/conf/article/{id}/status
     */
    public function status(Request $request, $id)
    {
        if (!$request->filled('statusId'))
            return $this->toJson(['message' => 'Запрос не верно построен'], 422);

        $article = Article::find($id);

        if (!$article)
            return $this->toJson(['message' => 'Научная статья не найдена'], 422);

        $article->status_id = $request->statusId;
        $article->save();

        return $this->toJson(['result' => true]);
    }

    /**
     * Изменение списка участников для статьи
     *
     * PUT /api/conf/article/{id}/members
     */
    public function putMembers(Request $request, $id)
    {
        abort(501);

        $article = Article::find($id);

        if (!$article)
            return $this->toJson(['message' => 'Научная статья не найдена'], 422);
    }

    /**
     * Удаление статьи
     *
     * DELETE /api/conf/article/{id}
     */
    public function delete($id)
    {
        $article = Article::find($id);

        if (!$article)
            return $this->toJson(['message' => 'Научная статья не найдена'], 422);

        $path = 'conference/' . $article->id . '_' . $article->created_at->timestamp;

        Storage::deleteDirectory($path);

        $article->delete();

        return $this->toJson(['result' => true]);
    }

    /**
     * Получение списка статей
     *
     * POST /api/conf/articles
     */
    public function articles(Request $request)
    {
        $articles = Article::query();

        if ($request->filled('name'))
            $articles->whereName($request->name);
        if ($request->filled('status') && (int) $request->status)
            $articles->whereStatusId($request->status);
        if ($request->filled('year') && (int) $request->year)
            $articles->whereYear('created_at', $request->year);

        if ($request->filled('sort')) {
            if ($request->sort === 'o')
                $articles->orderBy('name');  // По имени (А-Я)
            else if ($request->sort === 'n')
                $articles->orderByDesc('name');  // По имени (Я-А)

            if ($request->sort === 'co')
                $articles->orderBy('created_at');  // По дате (старые)
            else if ($request->sort === 'cn')
                $articles->orderByDesc('created_at');  // По дате (новые)
        }

        $articles = $articles->with(['section'])->get();
        $total = $articles->count();

        $articles = $this->modelsPaginator($articles, $request->perPage, $request->pageCount);

        if ($request->has('html')) {
            $articles = $articles->map(function ($elem) {
                return \View::make('console.conference.parts.article', [
                    'article' => $elem,
                ])->render();
            })->values();
        }

        return $this->toJson([
            'data' => $articles,
            'total' => $total,
        ]);
    }
}
