<?php

namespace App\Http\Controllers\Database\conf;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\conf\Section;

class SectionGuestController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Секция конференции / направление темы (для гостя)
    |--------------------------------------------------------------------------
    */

    /**
     * Получение списка тем
     *
     * POST /api/guest/conf/sections
     */
    public function sections()
    {
        $sections = Section::where('active', '=', 1)
            ->orderBy('order')
            ->get();

        return $this->toJson($sections);
    }
}
