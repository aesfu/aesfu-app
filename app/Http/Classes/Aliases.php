<?php

namespace App\Http\Classes;

use Illuminate\Support\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Pagination\LengthAwarePaginator;

trait Aliases
{
    /**
     * Ответ в формате JSON
     * 
     * @param mixed $data
     * @param int $code
     */
    protected function toJson(mixed $data = [], int $code = 200)
    {
        return response()->json(
            $data,
            $code,
            ['Content-type' => 'application/json;charset=utf-8'],
            JSON_UNESCAPED_UNICODE
        );
    }

    /**
     * Изменение временной зоны и формата даты
     * 
     * @param Carbon
     * @param string
     */
    protected function reformatDate($data, $format = null)
    {
        if ($data->timestamp <= 0) {
            return new Carbon(0);
        }

        $timezone = \Session::get('timezone') ?? '-7';

        $data->setTimezone('Etc/GMT' . $timezone);

        return $format ? $data->format($format) : $data;
    }

    /**
     * Collection pagination.
     *
     * @param Builder Eloquent
     * @param int per page
     * @param int page count
     * @param boolean with trashed
     */
    protected function paginator($model, null|int $perPage = 50, null|int $pageCount = 1, bool $trashed = false)
    {
        if (is_string($model)) {
            $model = (new $model())::query();
        }

        $perPage = $perPage ? intval($perPage) : 50;
        $pageCount = $pageCount ? intval($pageCount) : 1;

        $total = $model->count();
        $items = $model->limit($perPage)
            ->offset($perPage * $pageCount - $perPage);

        if (boolval($trashed))
            $items = $items->withTrashed();

        return collect([
            'total' => $total,
            'perPage' => $perPage,
            'pageCount' => $pageCount,
            'lastPage' => intval(ceil($total / $perPage)) <= $pageCount,
            'items' => $items->get(),
        ]);
    }

    /**
     * TODO: should be deprecated
     * Постраничная загрузка списка
     * 
     * @param Model - Eloquent модель
     * @param int - записей на страницу
     * @param int - номер страницы
     */
    protected function modelsPaginator($models, $perPage, $pageCount)
    {
        if (!$models || $models->count() === 0)
            return collect();

        $perPage = $perPage ?? 50;
        $pageCount = $pageCount ?? 1;

        $paginate = new LengthAwarePaginator(
            $models->forPage($pageCount, $perPage),
            $models->count(),
            $perPage,
            $pageCount,
            []
        );

        return collect($paginate->items());
    }
}