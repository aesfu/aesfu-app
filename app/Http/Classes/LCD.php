<?php

namespace App\Http\Classes;

use App\Models\mddb\Admatrix;

class LCD
{
    /*
    |--------------------------------------------------------------------------
    | Методика оценки Уровня Развития Компетентностей (УРК)
    |--------------------------------------------------------------------------
    |
    | LSD (УРК) - Level of Competence Development
    */

    /**
     * Априорная оценка доверия
     */
    protected $belief = 0.5;

    /**
     * Априорная оценка недоверия
     */
    protected $disbelief = 0.5;

    /**
     * Расчет значения доверия
     * для одного дистрактора по теореме Байеса
     */
    protected function beliefFactor($value)
    {
        $divisor = $this->belief * $value + $this->disbelief * (1 - $value);

        if ($divisor === 0.0) {
            return 0.0;
        }

        return $this->belief * $value / $divisor;
    }

    /**
     * Расчет значения недоверия
     * для одного дистрактора по теореме Байеса
     */
    protected function disbeliefFactor($value)
    {
        $divisor = ($this->disbelief * $value) + ($this->belief * (1 - $value));

        if ($divisor === 0.0) {
            return 0.0;
        }

        return $this->disbelief * $value / $divisor;
    }

    /**
     * Установка значения априорной оценки
     */
    public function setBelief($value)
    {
        if (!$value) {
            return;
        }

        $value = (float) $value;

        if (0.1 > $value || $value > 0.9) {
            return;
        }

        $this->belief = $value;
        $this->disbelief = 1 - $value;

        return $this;
    }

    /**
     * Расчет коэффициента уверенности
     * для набора экспертных оценок дистракторов
     * по методике Шортлиффа-Бьюкенена
     */
    public function confidence($weights)
    {
        $weights = collect($weights);

        if ($weights->count() === 0) {
            return 0.0;
        }

        $meansBelief = $weights->map(function ($elem) {
            return $this->beliefFactor($elem);
        });
        $meansDisbelief = $weights->map(function ($elem) {
            return $this->disbeliefFactor(1 - $elem);
        });

        $meanBelief = $meansBelief->sum() / $meansBelief->count();
        $meanDisbelief = $meansDisbelief->sum() / $meansDisbelief->count();

        $measureBelief = ($meanBelief - $this->belief) / (1 - $this->belief);
        $measureDisbelief = ($meanDisbelief - $this->disbelief) / (1 - $this->disbelief);

        return $measureBelief - $measureDisbelief;
    }

    /**
     * Расчет компетентностного профиля
     */
    public function profile($competences, $testQuests, $results)
    {
        /** -_- ПРЕДСТАВИМ, ЧТО ЗДЕСЬ НЕОБХОДИМЫЕ ПРОВЕРКИ */

        $factors = collect();

        $answersID = $testQuests->map(function ($elem) {
            return $elem->answers->map(function ($value) {
                return $value->id;
            });
        })->collapse();
        $adMatrix = Admatrix::whereIn('answer_id', $answersID)->get();

        // $competences = $competences->where('id', 1);

        foreach ($competences as $competence) {
            $quests = $testQuests->filter(function ($elem) use ($competence) {
                return $elem->competences->contains('id', $competence->id);
            });

            $totalWeights = collect();
            $modelWeights = collect();
            $competenceWeights = $adMatrix->where('competence_id', $competence->id);

            foreach ($quests as $quest) {
                /** ВЕСА ДЛЯ УЧЕНИКА */
                $resultAnswersID = $results->where('quest_id', $quest->id)
                    ->map(function ($elem) {
                        return $elem->answer_id;
                    });

                $weights = $competenceWeights->whereIn('answer_id', $resultAnswersID)
                    ->map(function ($elem) {
                        return $elem->value;
                    });

                $totalWeights->push($weights->count() > 0 ? $weights->sum() : 0);

                /** ВЕСА ДЛЯ ЭТАЛОНА */
                $questAnswersID = $quest->answers
                    ->map(function ($elem) {
                        return $elem->id;
                    });

                if ($quest->questtype->type === 0) {
                    $questAnswersID->pop();
                }

                $weights = $competenceWeights->whereIn('answer_id', $questAnswersID)
                    ->map(function ($elem) {
                        return $elem->value;
                    });

                if ($quest->questtype->type === 0)
                    $modelWeights->push($weights->sum());
                else
                    $modelWeights->push($weights->max());
            }

            $totalFactor = $this->confidence($totalWeights);
            $modelFactor = $this->confidence($modelWeights);

            $scaled = $modelFactor === 0.0 ? 0.0 : $totalFactor / $modelFactor;

            $scaled = $scaled > 1.0 ? 1.0 : ($scaled < -1.0 ? -1.0 : $scaled);

            $factors->push([
                'val' => (float) number_format($scaled, 3),       /** Scaled Value */
                'uc' => (float) number_format($totalFactor, 3),   /** User Coefficient Value */
                'mc' => (float) number_format($modelFactor, 3),   /** Model Coefficient Value */
                'comp' => $competence->id,                        /** Related Competence */
            ]);
        }

        return $factors;
    }

    public function CMKDprofile($competence, $quest, $results, $test)
    {
        /** -_- ПРЕДСТАВИМ, ЧТО ЗДЕСЬ НЕОБХОДИМЫЕ ПРОВЕРКИ */

        $factors = collect();

        $answersID = $quest->answers->map(function($value) { return $value->id; }); 
        $adMatrix = Admatrix::whereIn('answer_id', $answersID)->get();
        $totalWeights = collect();
        $modelWeights = collect();
        $competenceWeights = $adMatrix->where('competence_id', $competence->id);

                /** ВЕСА ДЛЯ УЧЕНИКА */
                $resultAnswersID = $results->where('quest_id', $quest->id)
                    ->map(function($elem) { return $elem->answer_id; });

                $weights = $competenceWeights->whereIn('answer_id', $resultAnswersID)
                    ->map(function($elem) { return $elem->value; });

                $totalWeights->push($weights->count() > 0 ? $weights->sum() : 0);

                /** ВЕСА ДЛЯ ЭТАЛОНА */
                $questAnswersID = $quest->answers
                    ->map(function($elem){ return $elem->id; });

                if ($quest->questtype->type === 0) $questAnswersID->pop();

                $weights = $competenceWeights->whereIn('answer_id', $questAnswersID)
                    ->map(function($elem){ return $elem->value; });

                if ($quest->questtype->type === 0) $modelWeights->push($weights->sum());
                else $modelWeights->push($weights->max());
                /**                  */ 

        $totalFactor = $this->confidence($totalWeights);
        $modelFactor = $this->confidence($modelWeights);

        $scaled = $modelFactor === 0.0 ? 0.0 : $totalFactor / $modelFactor;

        $scaled = $scaled > 1.0 ? 1.0 : ($scaled < -1.0 ? -1.0 : $scaled);

        $factors->push([
            'value' => (float)number_format($scaled, 3),       /** Scaled Value */
            'edelements' => $quest->edelements,                       /** Related quest */
            'quest_id' => $quest->id,
            'competence_id' => $competence->id,
            'test_id' => $test->id,
        ]);


        return $factors->collapse();
    }
}