<div class="ui card square">
    <div class="content">

        @canany(['admin', 'editor'])
            <div class="ui right floated">
                <a class="control" href="{{ route('editor.test.edit', [$test->id]) }}" title="Настройки теста">
                    <i class="cog icon"></i>
                </a>
            </div>
        @endcanany

        <div class="header">{{ $test->name }}</div>

        <div class="meta">
            <span class="category">{{ $test->option() }}</span>-
            <span class="category">до {{ $access_till }}</span>
        </div>

        <div class="description">

            @if ($test->userowner) <p>Создал(а): {{ $test->userowner->name() }}</p> @endif

            <p>Вопросов: {{ $test->quests->count() }}</p>

            @isset($test->discipline)
               <p>Дисциплина: {{ $test->discipline->name }}</p>
            @endisset

            @isset($test->timer)
               <p>Время на выполнение: {{ date('H:i:s', $test->timer) }} </p>
            @endisset
        </div>
    </div>

    <div class="extra content">
        <div class="left floated">
            <a class="control" href="{{ '/test/'.$test->id }}" title="Начать прохождение тетса">
                <i class="play icon"></i>
            </a>
        </div>

        <div class="right floated author">Создан: {{ $created_at }}</div>

    </div>
</div>
