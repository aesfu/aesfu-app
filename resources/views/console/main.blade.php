@extends('layouts.main', ['title' => config('app.name').' - Консоль'])

@section('content')

<!-- [DELETE] -->

<div class="ui stackable grid">
    @include('console.parts.menu')

    <div class="column" style="flex-grow:1;">

        <div class="ui one column grid">
            <div class="column">

                <table class="ui very basic unstackable table">
                    <thead>
                        <tr class="center aligned">
                            <th colspan="2">Общая статистика</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td class="ten wide"><i>Анкет и Тестов</i></td>
                            <td class="six wide center aligned">{{ App\Models\mddb\Test::all()->count() }}</td>
                        </tr>
                        <tr>
                            <td class="ten wide"><i>Пользователей</i></td>
                            <td class="six wide center aligned">{{ App\Models\User::all()->count() }}</td>
                        </tr>
                        <tr>
                            <td class="ten wide"><i>Количество пройденый тестов</i></td>
                            <td class="six wide center aligned">{{ App\Models\mddb\Basket::all()->count() }}</td>
                        </tr>
                    </tbody>
                </table>

                @if ((int)date('m') >= 9)
                    <table class="ui very basic unstackable table">
                        <thead>
                            <tr class="center aligned">
                                <th colspan="2">Научно-техническая конференция {{ $year }}</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td class="ten wide"><i>Участников</i></td>
                                <td class="six wide center aligned">{{ $members->count() }}</td>
                            </tr>
                            <tr>
                                <td class="ten wide"><i>Поданных статей</i></td>
                                <td class="six wide center aligned">{{ $articles->count() }}</td>
                            </tr>
                        </tbody>
                    </table>
                @endif

            </div>
        </div>
    </div>

</div>

@endsection
