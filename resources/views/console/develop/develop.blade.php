@extends('layouts.main', ['title' => config('app.name').' - Разработчики'])

@section('content')

<!-- [DELETE] -->

<div class="ui stackable grid">
    @include('console.parts.menu')

    <div class="column" style="flex-grow:1;">

        <div class="ui one column grid">
            <div class="column">

                <table class="ui very basic unstackable table">
                    <thead>
                        <tr>
                            <th class="center aligned">Основные данные</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td><a href="https://gitlab.com/aesfu/aesfu-app" target="_blank">Репозиторий проекта</a></td>
                        </tr>
                        <tr>
                            <td>
                                <a href="https://gitlab.com/aesfu/aesu-database/-/tree/master/api" target="_blank">REST API</a>
                                <span style="margin:0 4px;">|</span>
                                <a href="https://gitlab.com/aesfu/aesu-database/-/tree/master/schemas" target="_blank">Схемы БД</a>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <a href="/logs" target="_blank">Ошибки</a>:
                                <span style="margin:0 7.5px;">Laravel ( {{ $laravel->count() }} )</span>
                                <span>ReactJS ( {{ $react->count() }} )</span>
                            </td>
                        </tr>
                    </tbody>
                </table>

                <div class="ui divider"></div>

                <table class="ui very basic unstackable table">
                    <thead>
                        <tr>
                            <th class="center aligned">Разработчики приложения</th>
                        </tr>
                    </thead>
                    <tbody>

                       	@foreach ($develops as $develop)
                            <tr><td>{{ $develop->name() }}</td></tr>
                        @endforeach

                    </tbody>
                </table>

            </div>
        </div>

    </div>
</div>

@endsection
