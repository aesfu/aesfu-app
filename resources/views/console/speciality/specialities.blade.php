@extends('layouts.main', ['title' => config('app.name').' - Специальности'])

@section('content')

<!-- [DELETE] -->

<div class="ui stackable grid">
    @include('console.parts.menu')
    
    <div class="column" style="flex-grow:1;">
        
		@canany(['admin', 'editor'])
			<div class="ui one column grid">
				<div class="column" style="padding-bottom:0 !important;">

					{{-- КНОПКА СОЗДАТЬ --}}
					<div class="ui grid">
						<div class="one column right aligned row">
							<div class="column">
								<a class="ui positive tiny button" href="{{ route('editor.spec.new') }}">Создать</a>
							</div>
						</div>
					</div>

				</div>
			</div>
		@endcanany
        
        <div class="ui one column grid">
            <div class="column">
                
                <table class="ui very basic unstackable table">
                   	<thead>
                   		<tr>
                   			<th class="two wide">Код</th>
                   			<th class="thirteen wide">Наименование</th>
                   			<th class="one wide"></th>
                   		</tr>
                   	</thead>
                    <tbody id="models-list" page-count="1">
                        
                        @foreach ($views as $view)
                            {!! $view !!}
                        @endforeach
                        
                    </tbody>
                </table>
                
            </div>
        </div>
        
    </div>
</div>

@endsection