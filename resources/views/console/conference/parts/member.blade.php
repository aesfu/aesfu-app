<tr>
    <td class="fourteen wide">{{ $member->name() }}</td>
    <td class="center aligned one wide">
        <a class="control" href="{{ '/editor/member/'.$member->id }}" title="Просмотр участника">
            <i class="cog icon"></i>
        </a>
    </td>
</tr>
