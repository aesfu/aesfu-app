@extends('layouts.main', ['title' => config('app.name').' - Заявленные статьи'])

@section('content')

<div class="ui stackable grid">
    @include('console.parts.menu')

    <div class="column" style="flex-grow:1;">

        <div style="margin-bottom:16px;">
            <div class="ui form">

                <div class="fields inline">
                    <div class="field inline">
                        <label style="margin-right:4px;">Год:</label>

                        <div id="year" class="ui inline scrolling dropdown">
                            <div class="divider text">-</div>
                            <i class="dropdown icon"></i>

                            <div class="menu">

                                @for ($year = 2020; $year <= (int)date('Y'); $year++)
                                    <div class="item" data-value="{{ $year }}">
                                        <span class="text">{{ $year }}</span>
                                    </div>
                                @endfor

                            </div>
                        </div>
                    </div>

                    <div id="status-field" class="field inline" style="margin-left:8px;">
                        <label style="margin-right:4px;">Статус:</label>

                        <div id="status" class="ui inline scrolling dropdown">
                            <div class="divider text">-</div>
                            <i class="dropdown icon"></i>

                            <div class="menu">
                                <div class="item" data-value="0">Все</div>

                                @foreach ($statuses as $status)
                                    <div class="item" data-value="{{ $status->id }}">{{ $status->name }}</div>
                                @endforeach

                            </div>
                        </div>
                    </div>

                </div>

            </div>
        </div>

        <div style="margin-bottom: 16px;">
            <div id="tabs" class="ui top attached tabular menu">
                <div
                    class="item"
                    data-tab="0"
                    style="width:125px;justify-content:center;cursor:pointer;"
                >
                    Статьи
                </div>
                <div
                    class="item"
                    data-tab="1"
                    style="width:125px;justify-content:center;cursor:pointer;"
                >
                    Участники
                </div>
            </div>

            <div class="ui tab" data-tab="0"></div>
            <div class="ui tab" data-tab="1"></div>
        </div>

        <div class="ui one column grid">
            <div id="models" class="column">
                <table class="ui very basic unstackable table">
                    <tbody id="models-list" page-count="1"></tbody>
                </table>
            </div>
            <div id="btn-next" class="row centered" style="display:none;">
                <button class="ui button basic small" type="button">Загрузить еще</button>
            </div>
            <div id="loader" class="row centered" style="display:none;">
                <div class="ui active inline loader"></div>
            </div>
        </div>

    </div>
</div>

@endsection

@section('script')

<script type="text/javascript">
    const STORAGE_NAME = 'console_conference'
    var ls = JSON.parse(localStorage.getItem(STORAGE_NAME)) || { tab: '0' }

    /** ПОЛУЧЕНИЕ ДАННЫХ */

    $('#btn-next').click(function(event) {
        $('#loader').show();

        const url = ls.tab === '0' ? '/api/conf/articles' : '/api/conf/members'
        const pageCount = parseInt($('#models-list').attr('page-count'))
        const status = ls.tab === '0' && ls.status !== '0' ? ls.status : null

        $.ajax({
                method: 'POST',
                url: `${url}?html`,
                data: {
                    pageCount: pageCount,
                    year: ls.year,
                    status: status,
                    sort: 'cn',
                },
                headers: { "X-CSRF-TOKEN": $("meta[name='csrf-token']").attr('content') },
            })
            .done(data => {
                const modelsCount = $('#models-list').append(data.data)
                    .attr('page-count', pageCount + 1)
                    .children()
                    .length

                if (modelsCount >= data.total) $('#btn-next').hide()
                else $('#btn-next').show()
            })
            .fail((jqXHR, textStatus, errorThrown) => {
                console.log('$ jqXHR', jqXHR)
                console.log('$ textStatus', textStatus)
                console.log('$ errorThrown', errorThrown)
            })
            .always(() => {
                $('#loader').hide();
            })
    }).click()


    if (ls.tab === '1') $('#status-field').hide()

    /** СОБЫТИЯ КОМПОНЕНТОВ ПОИСКА */

    $('#year').dropdown('set selected', ls.year || new Date().getFullYear())
        .dropdown({
            onChange: function(value) {
                ls.year = value
                localStorage.setItem(STORAGE_NAME, JSON.stringify(ls))

                $('#models-list').attr('page-count', 1).empty()
                $('#btn-next').hide().click()
            }
        })

    $('#status').dropdown('set selected', ls.status || '0')
        .dropdown({
            onChange: function(value) {
                ls.status = value
                localStorage.setItem(STORAGE_NAME, JSON.stringify(ls))

                $('#models-list').attr('page-count', 1).empty()
                $('#btn-next').hide().click()
            }
        })

    $('#tabs').children(`[data-tab=${ls.tab}]`)
        .addClass('active')

    $('#tabs').children('.item').tab({
        onLoad: function(value) {
            ls.tab = value
            localStorage.setItem(STORAGE_NAME, JSON.stringify(ls))

            if (ls.tab === '0') $('#status-field').show()
            else $('#status-field').hide()

            $('#models-list').attr('page-count', 1).empty()
            $('#btn-next').hide().click()
        }
    })

</script>

@endsection
