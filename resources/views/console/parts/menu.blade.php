
<!-- [DELETE] -->

<div class="column" style="width:230px">
    <div class="ui secondary vertical menu">
        <a class="item" href="{{ route('console.main') }}"><b>Консоль</b></a>
        <a class="{{ Request::is('console/specialities') ? 'active ' : ''}}item" href="{{ route('console.spec.main') }}">Специальности</a>

        <a class="{{ Request::is('console/branches') ? 'active ' : ''}}item" href="{{ route('console.branch.main') }}">Факультативные группы</a>
        <a class="{{ Request::is('console/subgroups') ? 'active ' : ''}}item" href="{{ route('console.subgroup.main') }}">Подгруппы</a>
        <a class="{{ Request::is('console/disciplines') ? 'active ' : ''}}item" href="{{ route('console.disc.main') }}">Дисциплины</a>

        <a class="{{ Request::is('console/competences') ? 'active ' : '' }}item" href="{{ route('console.competence.main') }}">Компетенции</a>

        <a class="{{ Request::is('console/tests') ? 'active ' : ''}}item" href="{{ route('console.test.main') }}">Анкеты и тесты</a>
        <a class="{{ Request::is('console/quests') ? 'active ' : ''}}item" href="{{ route('console.quest.main') }}">Вопросы</a>


        {{-- glossary --}}
        {{--<div class="ui dropdown item">
            <i class="dropdown icon"></i>Словари

            <div class="menu">
                <a class="item disabled" href="#glossary">Задачи</a>
                <a class="item disabled" href="#glossary">Методологии</a>
                <a class="item" href="#glossary">Компетенции</a>
                <a class="item disabled" href="#glossary">Дидактичекие единицы</a>
            </div>
        </div>--}}

        <a class="{{ Request::is('console/users') ? 'active ' : ''}}item" href="{{ route('console.user.main') }}">Пользователи</a>
        <a class="{{ Request::is('console/passed') ? 'active ' : ''}}item" href="{{ route('console.passed.main') }}">Пройденные тесты</a>

        <div class="ui divider"></div>

        <a class="{{ Request::is('console/articles') ? 'active ' : ''}}item" href="{{ route('console.conference.main') }}">Конференция</a>

        {{--<a class="item disabled" href="#help">Справочник</a>--}}

        @can('develop')
            <div class="ui divider"></div>

            <a class="{{ Request::is('console/develop') ? 'active ' : ''}}item" href="{{ route('console.develop.main') }}">Разработчикам</a>
        @endcan

    </div>
</div>