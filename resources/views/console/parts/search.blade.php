{{-- ШАБЛОННЫЙ ПОИСК ДЛЯ ВСЕХ СТРАНИЦ --}}
<script type="text/javascript">
    {{-- ПОЛУЧИТЬ СЛЕДУЮЩИЕ МОДЕЛИ [get_models] --}}
    const gm = function(){
        let btn = $(this);
		let models = $("#models");
        let page = parseInt($("#models-list").attr("page-count"));
		
        btn.addClass("loading");
        
        $.ajax({
            method: "POST",
            url: models.attr("route-next"),
            headers: { "X-CSRF-TOKEN": $("meta[name='csrf-token']").attr('content') },
            dataType: "json",
            data: { page: page },
			complete: function(){
                $("#models-list").attr("page-count", ++page);
                btn.removeClass("loading").closest('.row.centered').show();
			},
            success: function(data){
				let list = models.find("#models-list");
				
                data.views.forEach(function(val){
                    list.append(val);
                });

                if (data.last){
                    btn.closest('.row.centered').remove(); 
                }
            },
			error: (jq, text, err) => de(jq),
        });
    };
    {{-- ПОИСК [search_models] --}}
    const sm = function(){
		let models = $("#models");
        let input = $("#search-text");
        
        input.closest(".ui.input").addClass("loading");
		
        input.val(input.val().trim());
        $("#models-next").closest('.row.centered').remove();
		
        $.ajax({
            method: "POST",
            url: models.attr("route-search"),
            headers: { "X-CSRF-TOKEN": $("meta[name='csrf-token']").attr('content') },
            data: {
                param: input.val()
            },
            dataType: "json",
            complete: function(){
                input.closest(".ui.input").removeClass("loading");
            },
            success: function(data){
				let list = models.find("#models-list");
                
                list.empty();
                
                if (!data.views || data.views.length == 0){
                    list.html('<tr><td>Поиск не дал результатов...</td></tr>');
                }
                else {
                    data.views.forEach(function(val){
                        list.append(val);
                    });
                }
            },
			error: (jq, text, err) => de(jq),
        });
    };
    
    $("#models-next").click(gm).click();
    $("#search-text").keydown((ev) => { if (ev.keyCode == 13) sm(); });
    $("#search-icon").click(sm);
</script>