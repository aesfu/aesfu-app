@extends('layouts.main', ['title' => config('app.name').' - Пользователи'])

@section('content')

<!-- [DELETE] -->

<div class="ui stackable grid">
    @include('console.parts.menu')
    
    <div class="column" style="flex-grow:1;">
        
        <div class="ui one column grid">
            <div class="column" style="padding-bottom:0 !important;">
                
                {{-- ФОРМА ПОИСКА --}}
                <div class="ui form">
                    
                    {{-- СОРТИРОВКА --}}
                    <div class="inline left aligned fields">
                        <div class="field">
                            <div class="ui dropdown" tabindex="-1">
                                <i class="sort amount down icon"></i>
                                <span>Сортировать</span>
                                <div class="menu transition hidden" tabindex="-1">
                                    <a class="item{{ Request::input('sort') == 'nm' ? ' active selected' : '' }}" href="{{ route('console.user.main', ['sort' => 'nm']) }}">По имени</a>
                                    <a class="item{{ Request::input('sort') == null ? ' active selected' : '' }}" href="{{ route('console.user.main') }}">Дата создания (новые)</a>
                                    <a class="item{{ Request::input('sort') == 'do' ? ' active selected' : '' }}" href="{{ route('console.user.main', ['sort' => 'do']) }}">Дата создания (старые)</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                    {{-- ПОИСКОВА СТРОКА --}}
                    <div class="field">         
						<div class="ui fluid icon input">
							<input id="search-text" type="text" placeholder="Ф.И.О.">
							<i id="search-icon" class="search link icon"></i>
						</div>
                    </div>
                    
                </div>
            </div>
        </div>
        
        <div class="ui one column grid">     
            <div id="models" class="column" route-next="{{ route('console.user.next', ['sort' => Request::input('sort')]) }}" route-search="{{ route('console.user.search', ['sort' => Request::input('sort')]) }}">
                <table class="ui very basic unstackable table">
                   	<thead>
                   		<tr>
                   			<th class="eight wide">Пользователь</th>
                   			<th class="three wide">Группа</th>
                   			<th class="four wide">Должность</th>
                   			<th class="one wide"></th>
                   		</tr>
                   	</thead>
                    <tbody id="models-list" page-count="1"></tbody>
                </table>
            </div>
            
            <div class="row centered">
                <button id="models-next" class="ui button basic small" type="button">Загрузить еще</button>
            </div>
        </div>
        
    </div>
</div>

@endsection

@section('script')

@include('console.parts.search')

@endsection
