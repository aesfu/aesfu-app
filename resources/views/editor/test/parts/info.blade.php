<div class="ui segment basic left aligned">         
    <table class="ui very basic unstackable table">
        <tbody>

            <tr>
                <td><b>Дисциплина</b></td>
                <td class="right aligned">
                    <button id="btn-pairs" class="ui icon basic small button">
                        <i class="pencil icon"></i>
                    </button>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    {{ $test->discipline ? $test->discipline->name : '-' }}
                </td>
            </tr>

            <tr><td colspan="2"><b>Наименование</b></td></tr>
            <tr><td colspan="2">{{ $test->name }}</td></tr>

            <tr>
                <td class="eight wide"><b>Тип теста</b></td>
                <td class="eight wide"><b>Таймер (секунд)</b></td>
            </tr>
            <tr>
                <td>{{ $test->option() }}</td>
                <td>{{ $test->timer ?? '-' }}</td>
            </tr>

            <tr><td colspan="2"><b>Описание</b></td></tr>
            <tr><td colspan="2">{!! $test->portrait ?? '-' !!}</td></tr>

            <tr><td colspan="2"><b>Владелец</b></td></tr>
            <tr><td colspan="2">{{ $test->userowner ? $test->userowner->name() : '-' }}</td></tr>

        </tbody>
    </table>
</div>
