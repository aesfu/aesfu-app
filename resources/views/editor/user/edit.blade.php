@extends('layouts.main', ['title' => config('app.name').' - Пользователь - '.$user->name()])

@section('content')

<div class="ui one column stackable grid">

    <div class="sixteen wide tablet twelve wide computer centered column">

        <div class="ui tiny breadcrumb">
            <a class="section bread" href="{{ route('console.main') }}">Консоль</a>
            <i class="right angle icon divider"></i>
            <a class="section bread" href="{{ route('console.user.main') }}">Пользователи</a>
            <i class="right angle icon divider"></i>
            <div class="section active">#{{ $user->id }}</div>
        </div>

        <div class="ui divider"></div>

        <div class="ui fluid accordion">
            <div class="title active">
                <i class="dropdown icon"></i>
                <b style="font-size:1.25rem;">Основные параметры</b>
            </div>

            <div class="content active">

                @include('editor.user.parts.edit')

            </div>
        </div>

        <div class="ui divider"></div>

        <div class="ui fluid accordion">
            <div class="title">
                <i class="dropdown icon"></i>
                <b style="font-size:1.25rem;">Пройденные тесты</b>
            </div>

            <div class="content">

                @include('editor.user.parts.passed')

            </div>
        </div>

        @can('admin')
            <div class="ui divider"></div>

            <div class="ui fluid accordion">
                <div class="title">
                    <i class="dropdown icon"></i>
                    <b style="font-size:1.25rem;">Настройка прав</b>
                </div>

                <div class="content">

                    @include('editor.user.parts.permission')

                </div>
            </div>
        @endcan

    </div>
</div>


@endsection

@section('script')

<script type="text/javascript">
    @can('admin')
        {{-- КНОПКА УДАЛИТЬ [button_delete] --}}
        const bd = function(){
            let val = confirm("Удаление пользователя постоянно и приведет к очистке всех данных, связанных с ним. Вы уверены, что хотите удалить пользователя?");

            if (val) $("#delete-form").attr("action", "{{ route('editor.user.delete', [$user->id]) }}").submit();
        };
        {{-- СБРОСИТЬ ПАРОЛЬ [reset_password] --}}
        const rp = function(){
            $("#btn-reset").addClass("loading");

            $.ajax({
                method: "POST",
                url: "{{ route('editor.user.reset', [$user->id]) }}",
                headers: {
                    "X-CSRF-TOKEN": $("meta[name='csrf-token']").attr('content'),
                },
                dataType: "json",
                complete: function(){
                    $("#btn-reset").removeClass("loading");
                },
                success: function(){
                    $("#btn-reset").addClass("positive icon").off("click").html('<i class="check icon"></i>');
                },
                error: function(jq, text, err){
                    $('#message').children(".ui.header").text(jq.responseJSON.header);
                    $('#message').children("p").text(jq.responseJSON.message);
                    $('#message').closest(".grid").slideDown(50);
                },
            });
        };

        {{-- ОБНОВИТЬ ПРАВА [update_permissions] --}}
        const up = function(ev){
            ev.preventDefault();

            $("#btn-permission").addClass("loading");

            $.ajax({
                method: "POST",
                url: "{{ route('editor.user.permission', $user->id) }}",
                headers: {
                    "X-CSRF-TOKEN": $("meta[name='csrf-token']").attr('content'),
                },
                dataType: "json",
                data: $(this).serialize(),
                complete: function(){
                    $("#btn-permission").removeClass("loading");
                },
                success: function(){
                    $("#btn-permission").addClass("positive icon").off("click").html('<i class="check icon"></i>');
                },
                error: function(jq, text, err){
                    $('#message').children(".ui.header").text(jq.responseJSON.header);
                    $('#message').children("p").text(jq.responseJSON.message);
                    $('#message').closest(".grid").slideDown(50);
                },
            });
        };

        $("#delete-item").click(bd);
        $("#fm-permission").submit(up);
        $("#permissions").dropdown("set selected", {!! $has !!});
        $("#btn-reset").click(function(){ if (confirm("Вы уверены, что хотите сбросить пароль данного пользователя?")) rp() });
    @endcan

</script>

@endsection
