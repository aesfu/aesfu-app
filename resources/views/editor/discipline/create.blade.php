@extends('layouts.main', ['title' => config('app.name').' - Создание дисциплины'])

@section('style')

<link rel="stylesheet" type="text/css" href="{{ asset('css/editor/simditor.css') }}" />

@endsection

@section('content')

<div class="ui stackable centered grid">
    
    <div class="twelve wide column">
        
        <div class="ui tiny breadcrumb">
            <a class="section bread" href="{{ route('console.main') }}">Консоль</a>
            <i class="right angle icon divider"></i>
            <a class="section bread" href="{{ route('console.disc.main') }}">Дисциплины</a>
            <i class="right angle icon divider"></i>
            <div class="section active">Создать</div>
        </div>
        
        <div class="ui divider"></div>
        
        @include('editor.discipline.parts.edit')
        
    </div>
</div>

@endsection

@section('script')

@include('editor.discipline.parts.script')

<script type="text/javascript" src="{{ asset('js/editor/module.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/editor/hotkeys.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/editor/simditor.min.js') }}"></script>
<script type="text/javascript">
    $("input[type=text], #status").click(function(){
        $(this).closest(".field")
               .removeClass("error")
               .children(".ui.basic.label")
               .slideUp(50, function(){ $(this).remove(); });
    });
    
    let editor = new Simditor({
        toolbar: [
            "bold", "italic", "underline", "strikethrough", "color", "|",
            "ol", "ul", "|",
            "indent", "outdent", "|",
             "link", "hr"
        ],
        textarea: $("#portrait"),
        placeholder: "Описание дисциплины"
    });
    
    $("#btn-save").click(function(){ $(this).addClass("loading") });
    $("#status").dropdown("set selected", "{{ old('status') }}");
</script>

@endsection
