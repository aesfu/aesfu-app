<form id="fm-task" class="ui form" method="post">
    <div class="field">
        
        <table id="tb-tasks" class="ui very basic unstackable table">
            <thead>
               	<tr>
                    <th class="one wide center aligned" style="padding-left:5px;">#</th>
                    <th class="fourteen wide">Наименование</th>
                    <th class="center aligned one wide">
                        
                        @if ($edit)
                            <button id="btn-add-branch" class="ui icon mini positive button" type="button" data-content="Добавить группу"  data-position="left center">
                                <i class="plus icon"></i>
                            </button>
                        @endif
                        
                    </th>
               	</tr>
            </thead>
            
            <tbody>
                
                @foreach ($tasks as $task)
                    <tr>
                        <td id="count" class="center aligned">{{ $task->pivot->position }}</td>
						
                        <td colspan="2">{{ $task->name }}</td>

                        {{--<td class="center aligned two wide">

                            @if ($edit)
                                <button id="btn-remove" class="ui icon basic negative mini button" type="button">
                                    <i class="trash icon"></i>
                                </button>
                            @else
                                <a class="control disabled" href="{{ route('editor.quest.edit', [$quest->id]) }}" title="Редактировать вопрос">
                                    <i class="cog icon"></i>
                                </a>
                            @endif

                        </td>--}}
                    </tr>
                @endforeach
                
            </tbody>
        </table>
        
    </div>
</form>
