<div class="ui segment basic left aligned">
    
    @php
        $toEdit = Request::route()->getName() == 'editor.branch.pairs';
    @endphp
    
    {{-- ФОРМА ТОЛЬКО СОЗДАЕТ ГРУППУ, ОБНОВЛЕНИЕ ПРОИСХОДИТ АСИНХРОННО --}}
    <form class="ui form" action="{{ $toEdit ? route('editor.branch.update', ['id' => $branch->id]) : route('editor.branch.create') }}" method="POST">
        @csrf

        {{-- НАПРАВЛЕНИЯ ПОДГОТОВКИ --}}
        <div class="field{{ $errors->has('speciality') ? ' error' : ''}}">
            <label>Направление подготовки</label>
            <div id="speciality" class="ui fluid dropdown selection" tabindex="0">
                <select name="speciality">
                    <option value="">Направление подготовки</option>
                    
                    @foreach($specs as $spec)
                        <option value="{{ $spec->id }}">{{ $spec->name() }}</option>
                    @endforeach
                    
                </select>
                <i class="dropdown icon"></i>
                <div class="default text">Направление подготовки</div>
                <div class="menu transition hidden" tabindex="-1">
                   
                    @foreach($specs as $spec)
                        <div class="item" data-value="{{ $spec->id }}">{{ $spec->name()  }}</div>
                    @endforeach
                    
                </div>
            </div>
            
            @if ($errors->has('speciality'))
                <div class="ui basic red pointing prompt label transition">
                    {{ $errors->first('speciality') }}
                </div>
            @endif
            
        </div>

        {{-- НАИМЕНОВЕНИЕ --}}
        <div class="field{{ $errors->has('name') ? ' error' : ''}}">
            <label>Наименование</label>
            <input name="name" type="text" value="{{ $branch->name ?? old('name') }}" placeholder="Наименование" autocomplete="off">
            
            @if ($errors->has('name'))
                <div class="ui basic red pointing prompt label transition">
                    {{ $errors->first('name') }}
                </div>
            @endif
            
        </div>
        
        <div class="ui two column grid">

            <div class="left aligned column">
                <div class="column">
                    
                    <button id="btn-save" class="ui primary button" type="submit">{{ $toEdit ? 'Сохранить' : 'Создать' }}</button>
                    
                </div>
            </div>

            <div class="right aligned column">
                <div class="row">

                    @if ($toEdit)
                        <button id="btn-delete" class="ui negative button" type="button">Удалить</button>
                        <button id="btn-pairs" class="ui button" type="button">Отмена</button>
                    @else
                        <a class="ui button" href="{{ route('console.branch.main') }}">Отмена</a>
                    @endif

                </div>
            </div>

        </div>

    </form>
    
</div>
