<form class="ui form" method="post">
    <div class="field">

        <table class="ui very basic unstackable table">
            <thead>
                <tr>
                    <th class="one wide center aligned" style="padding-left:5px;">#</th>
                    <th class="fourteen wide">Наименование</th>
                    <th class="center aligned one wide"></th>
                </tr>
            </thead>

            <tbody>

                @if ($disciplines->count() == 0)
                    <tr>
                        <td class="center aligned">-</td>
                        <td>-</td>
                        <td class="center aligned"></td>
                    </tr>
                @endif

                @foreach ($disciplines as $key => $discipline)
                    <tr>
                        <td id="count" class="center aligned">{{ $key + 1 }}</td>

                        <td>
                            {{ $discipline->name }}
                        </td>

                        <td class="center aligned two wide">
                            <a class="control" href="{{ route('editor.disc.edit', [$discipline->id]) }}" title="Настройки дисциплины">
                                <i class="cog icon"></i>
                            </a>
                        </td>
                    </tr>
                @endforeach

            </tbody>
        </table>

    </div>
</form>
