<div class="ui segment basic left aligned">             
    <table class="ui very basic unstackable table">
        <tbody>

            <tr>
                <td><b>Направление подготовки</b></td>
                <td class="right aligned">
                    <button id="btn-pairs" class="ui icon basic small button">
                        <i class="pencil icon"></i>
                    </button>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    {{ $branch->speciality->name() }}
                </td>
            </tr>

            <tr><td colspan="2"><b>Наименование</b></td></tr>
            <tr><td colspan="2">{{ $branch->name }}</td></tr>

        </tbody>
    </table>
</div>
