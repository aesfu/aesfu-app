<script type="text/javascript">
    {{-- КЛИК ПО КНОПКЕ ДОБАВИТЬ ПОЛЬЗОВАТЕЛЯ [add_row] --}}
    const ar = function(holder){
        let body = holder.find("table > tbody");
        let model = holder.attr("model");
        let count = body.children("tr").length;

        let row = $('<tr>', {
            html: [
                $("<td>", {
                    id: "count",
                    class: "center aligned",
                    html: count + 1
                }), 
                $("<td>", {
                    class: "left aligned",
                    html: $("<div>", {
                        id: "search",
                        class: "ui fluid search selection dropdown",
                        html: [
                            '<i class="dropdown icon"></i>',
                            '<input id="search-text" class="search" autocomplete="off" tabindex="0">',
                            '<input type="hidden" name="' + model + '[]">',
                            '<div class="default text">Ф.И.О.</div>',
                            '<div class="menu" tabindex="-1"></div>'
                        ]
                    })
                }), 
                $("<td>", {
                    class: "right aligned two wide",
                    html: '<button id="btn-remove" class="ui icon basic negative mini button" type="button"><i class="trash icon"></i></button>'
                })
            ]
        });

        body.append(row);
        
        let route = holder.attr("route-search");

		row.find("#search").dropdown({ onChange: function(){ $(this).find(".menu").empty(); } });
        row.find("#search-text").on("input", (ev) => { ism(ev, route); });
        row.find("#btn-remove").click(function(){ $(this).parents("tr").remove(); });
    };
    {{-- ПОИСК ПОЛЬЗОВАТЕЛЯ [input_search_models] --}}
    const ism = function(ev, route){
        let text = $(ev.target).val().trim();
        
        if (text.length < 3) return;
        
        let menu = $(ev.target).parents("#search").find(".menu");
        
        $.ajax({
            method: "post",
            url: route,
            headers: {
                "X-CSRF-TOKEN": $("meta[name='csrf-token']").attr('content'),
            },
            data: {
                param: text
            },
            dataType: "json",
            success: function(data){
                menu.empty();
                data.views.forEach(function(val){
                    menu.append(val);
                });
            },
			error: (jq) => de(jq),
        });
    };
    {{-- ДОБАВИТЬ СОБЫТИЯ К ФОРМЕ И ТАБЛИЦЕ [add_events] --}}
    const ae = function(holder){		
        holder.find("#btn-add").click(() => ar(holder));
        
		let rows = holder.find("table > tbody > tr");
		if (rows.length == 0){
			holder.find("#btn-add").click();
		}
        rows.each(function(){
            $(this).find("#btn-remove").click(function(){
                $(this).parents("tr").remove();
            });
        });
    };
</script>