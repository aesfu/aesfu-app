@extends('layouts.main', ['title' => config('app.name').' - Создание компетенции'])

@section('content')

<div class="ui stackable centered grid">
    
    <div class="twelve wide column">
        
        <div class="ui tiny breadcrumb">
            <a class="section bread" href="{{ route('console.main') }}">Консоль</a>
            <i class="right angle icon divider"></i>
            <a class="section bread" href="{{ route('console.spec.main') }}">Специальности</a>
            <i class="right angle icon divider"></i>
            <div class="section active">Создать</div>
        </div>
        
        <div class="ui divider"></div>
        
        @include('editor.speciality.parts.edit')
        
    </div>
</div>

@endsection

@section('script')

<script type="text/javascript">
    $("input[type=text]").click(function(){
        $(this).closest(".field")
               .removeClass("error")
               .children(".ui.basic.label")
               .slideUp(50, function(){ $(this).remove(); });
    });
    
    $("#btn-save").click(function(){ $(this).addClass("loading") });
</script>

@endsection
