<form class="ui form" method="post">
    <div class="field">

        <table class="ui very basic unstackable table">
            <thead>
                <tr>
                    <th class="one wide center aligned" style="padding-left:5px;">#</th>
                    <th class="fourteen wide">Наименование</th>
                    <th class="center aligned one wide"></th>
                </tr>
            </thead>

            <tbody>

                @if ($member->articles->count() == 0)
                    <tr>
                        <td class="center aligned">-</td>
                        <td>-</td>
                        <td class="center aligned"></td>
                    </tr>
                @endif

                @foreach ($member->articles as $key => $article)
                    <tr>
                        <td id="count" class="center aligned">{{ $key + 1 }}</td>

                        <td>{{ $article->name }}</td>

                        <td class="center aligned">
                            <div class="ui right floated">
                                <a class="control" href="{{ '/editor/article/'.$article->id }}" title="Просмотр статьи">
                                    <i class="cog icon"></i>
                                </a>
                            </div>
                        </td>
                    </tr>
                @endforeach

            </tbody>
        </table>
    </div>
</form>
