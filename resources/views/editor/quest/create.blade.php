@extends('layouts.main', ['title' => config('app.name').' - Создание вопроса'])

@section('style')

<link rel="stylesheet" type="text/css" href="{{ asset('css/editor/simditor.css') }}" />
<link rel="stylesheet" type="text/css" href="{{ asset('css/table/jquery-ui.css') }}" />

@endsection

@section('content')

<div class="ui stackable centered grid">
    
    <div class="twelve wide column">
        
        <div class="ui tiny breadcrumb">
            <a class="section bread" href="{{ route('console.main') }}">Консоль</a>
            <i class="right angle icon divider"></i>
            <a class="section bread" href="{{ route('console.quest.main') }}">Вопросы</a>
            <i class="right angle icon divider"></i>
            <div class="section active">Создать</div>
        </div>
        
        <div class="ui divider"></div>
        
        @include('editor.quest.parts.edit')
        
    </div>
</div>

@endsection

@section('script')

@include('editor.quest.parts.script')

<script type="text/javascript">
    $("input[type=text], #type").click(function(){
        $(this).closest(".field")
               .removeClass("error")
               .children(".ui.basic.label")
               .slideUp(50, function(){ $(this).remove(); });
    });
    
    $("#btn-save").click(function(){ $(this).addClass("loading") });
    
    ae();
    $(".ui.segment form").submit(vf);
    $("#type").dropdown("set selected", "{{ old('type') }}");
</script>

@endsection
