<!DOCTYPE html>
<html lang="{{ config('app.locale') }}" version="{{ config('app.version') }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name') }}</title>
    <link rel="icon" href="{{ asset('favicon.ico') }}">

    <link href="{{ asset('css/semantic.min.css') }}" rel="stylesheet">

    @if (Auth::check())
    @php
        $user = \Auth::user()->load(['userstatus','permissions']);
        if ($user->branch)
            $user->branch->setHidden(['speciality_id'])->speciality;
        $user->permissions = $user->permissions->map(function($elem) {
            return $elem->setHidden(['id','guard_name','created_at','updated_at','pivot']);
        });
        $user->_csrf_token = csrf_token();
    @endphp
    <script id="user-data" type="application/json">@json($user, JSON_UNESCAPED_UNICODE)</script>
    @endif

</head>
<body>
    <div id="root">
        <div class="ui top inverted large borderless menu" style="border-radius: 0px;"></div>
    </div>

    @viteReactRefresh
    @vite('resources/src/pages/App/App.jsx')
</body>
</html>