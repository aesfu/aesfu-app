<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>403 Forbidden</title>

    @include('errors.style')
</head>
<body>
    <div class="wrapper">
        <div class="box">
            <h1 class="box__header">403 Forbidden</h1>
            <div class="box__message">
                <p>{{ $exception->getMessage() }}</p>
            </div>
            <a class="box__button" href="/">Домой</a>
        </div>
    </div>
</body>
</html>