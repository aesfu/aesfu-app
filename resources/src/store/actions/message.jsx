export const MESSAGE_BOX = 'MESSAGE_BOX'

export const box = data => ({
    type: MESSAGE_BOX,
    data: data,
})
