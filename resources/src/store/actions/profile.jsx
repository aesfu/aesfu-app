export const PROFILE_STATUS = 'PROFILE_STATUSES'
export const PROFILE_BRANCHES = 'PROFILE_BRANCHES'
export const PROFILE_BASKET = 'PROFILE_BASKETS'

export const statuses = (data, loaded = true) => ({
    type: PROFILE_STATUS,
    data: {
        data: data || [],
        loaded: Boolean(loaded),
    },
})

export const branches = (data, loaded = true) => ({
    type: PROFILE_BRANCHES,
    data: {
        data: data || [],
        loaded: Boolean(loaded),
    },
})

export const baskets = (data, loaded = true) => ({
    type: PROFILE_BASKET,
    data: {
        data: data || [],
        loaded: Boolean(loaded),
    },
})
