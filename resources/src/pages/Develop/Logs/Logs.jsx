/**
 * Страница логирования ошибок и исключений Laravel и ReactJS
 */
import React from 'react'
import ReactDOM from 'react-dom'
import { Grid, Button, Placeholder } from 'semantic-ui-react'
import axios from 'utils/axios.jsx'

import { APP_NAME } from 'utils/auth.jsx'
import Log from './Log/Log.jsx'

const STORAGE_NAME = 'logs'

class Logs extends React.Component {
    state = {
        laravel: {
            data: [],
            color: "#ff9696",
            loading: true,
        },
        react: {
            data: [],
            color: "#78b4fa",
            loading: true,
        },
        selection: localStorage.getItem(STORAGE_NAME) || 'laravel',
        delete: false,
    }

    componentDidMount() {
        const title = this.state.selection === 'laravel' ? 'Laravel' : 'ReactJS'
        document.title = `${title} - Ошибки - ${APP_NAME}`

        Promise.all([
            axios.get('/api/laravel/errors'),
            axios.get('/api/react/errors'),
        ])
            .then(response => {
                const laravel = response[0].data
                const reactJS = response[1].data

                this.setState(state => ({
                    laravel: {
                        ...state.laravel,
                        data: laravel,
                        loading: false,
                    },
                    react: {
                        ...state.react,
                        data: reactJS,
                        loading: false,
                    }
                }))
            })
            .catch(() => {
                this.setState(state => ({
                    laravel: {
                        ...state.laravel,
                        loading: false,
                    },
                    react: {
                        ...state.react,
                        loading: false,
                    }
                }))
            })
    }

    appClickHandler = name => {
        if (this.state.selection === name) return

        const title = name === 'laravel' ? 'Laravel' : 'ReactJS'
        document.title = `${title} - Ошибки - ${APP_NAME}`

        this.setState({ selection: name })
        localStorage.setItem(STORAGE_NAME, name)
    }

    deleteClickHandler = index => {
        if (this.state.delete) return

        if (!confirm('Вы уверенны в удалении ошибки?')) return

        this.setState({ delete: true })

        const state = { ...this.state }
        const { selection } = state

        let errors = state[selection].data
        const error = errors[index]

        axios.delete(`/api/${selection}/error/${error.id}`)
            .then(_ => {
                errors.splice(index, 1)

                state[selection].data = errors
                state.delete = false

                this.setState(state)
            })
            .catch(() => {
                this.setState({ delete: false })
            })
    }

    deleteAllClickHandler = _ => {
        if (this.state.delete) return

        if (!confirm('Вы уверенны в удалении всех ошибок?')) return

        this.setState({ delete: true })

        const state = { ...this.state }
        const { selection } = state

        axios.delete(`/api/${selection}/errors`)
            .then(_ => {
                state[selection].data = []
                state.delete = false

                this.setState(state)
            }).catch(() => { })
    }

    render() {
        const { selection, laravel, react } = this.state

        const error = this.state[selection]

        const content = error.loading ? (
            <Grid.Column>
                <Placeholder fluid>
                    <Placeholder.Paragraph>
                        <Placeholder.Line />
                    </Placeholder.Paragraph>
                </Placeholder>
            </Grid.Column>
        ) : (
            <Log
                error={error}
                onDelete={this.deleteClickHandler}
                onDeleteAll={this.deleteAllClickHandler} />
        )

        return (
            <Grid style={{ maxWidth: "880px", margin: "0 auto", paddingBottom: "100px" }}>
                <Grid.Row columns={2} style={{ paddingBottom: 0 }}>
                    <Grid.Column mobile={16} tablet={8} computer={8} style={{ padding: "2.5px 7px" }}>
                        <Button
                            basic
                            fluid
                            style={selection === 'laravel' ? { boxShadow: `0 0 0 1.5px ${laravel.color} inset` } : null}
                            onClick={_ => this.appClickHandler('laravel')} >
                            Laravel ( {laravel.data ? laravel.data.length : '?'} )
                        </Button>
                    </Grid.Column>
                    <Grid.Column mobile={16} tablet={8} computer={8} style={{ padding: "2.5px 7px" }}>
                        <Button
                            basic
                            fluid
                            style={selection === 'react' ? { boxShadow: `0 0 0 1.5px ${react.color} inset` } : null}
                            onClick={_ => this.appClickHandler('react')} >
                            ReactJS ( {react.data ? react.data.length : '?'} )
                        </Button>
                    </Grid.Column>
                </Grid.Row>

                <Grid.Row columns={1} style={{ paddingTop: "0" }}>
                    {content}
                </Grid.Row>
            </Grid>
        )
    }
}

ReactDOM.render(<Logs />, document.getElementById('root'))
