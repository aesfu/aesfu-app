/**
 * Таблица оцениваемых компетенций профиля
 */
import React from 'react'
import { Table, Input, Checkbox, Popup } from 'semantic-ui-react'

const DataTable = props => {
    const { profile, displayChange, checkboxChange } = props

    const rows = profile.map((elem, index) => (
        <Table.Row key={ `${index}${elem.id}` }>
            <Table.Cell>
                <Input
                    fluid
                    value={ elem.display || '' }
                    placeholder={ index + 1 }
                    onChange={ event => displayChange(event, index) }
                    input={ <input type="text" style={{ padding: "6px 4px", textAlign: "center" }} /> } />
            </Table.Cell>
            <Table.Cell>
                <Checkbox
                    checked={ elem.v }
                    label={ elem.name }
                    onChange={ (_, data) => checkboxChange(_, data, index) } />
            </Table.Cell>
            <Table.Cell textAlign="center">{ elem.uc }</Table.Cell>
            <Table.Cell textAlign="center">{ elem.val }</Table.Cell>
        </Table.Row>
    ))

    return (
        <Table basic="very" unstackable compact>
            <Table.Header>
                <Table.Row>
                    <Table.HeaderCell textAlign="center" style={{ minWidth: "70px" }}>
                        <Popup
                            wide
                            position="top left"
                            content="Обозначение на диаграмме"
                            trigger={ <span style={{ display: "block" }}>#</span> } />
                    </Table.HeaderCell>
                    <Table.HeaderCell width={ 11 }>Компетентность</Table.HeaderCell>
                    <Table.HeaderCell width={ 2 } textAlign="center">
                        <Popup
                            wide
                            position="top center"
                            content="Коэффициент без масштабирования"
                            trigger={ <span style={{ display: "block" }}>БМ</span> } />
                    </Table.HeaderCell>
                    <Table.HeaderCell width={ 2 } textAlign="center">
                        <Popup
                            wide
                            position="top right"
                            content="Масштабированный коэффициент"
                            trigger={ <span style={{ display: "block" }}>М</span> } />
                    </Table.HeaderCell>
                </Table.Row>
            </Table.Header>
            <Table.Body>
                { rows }
            </Table.Body>
        </Table>
    )
}

export default DataTable
