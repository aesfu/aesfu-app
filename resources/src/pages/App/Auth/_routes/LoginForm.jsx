/**
 * Форма авторизации
 */
import React from 'react'
import { Grid, Segment, Form, Divider, Icon } from 'semantic-ui-react'
import axios from 'utils/axios.jsx'

import { APP_NAME } from 'utils/auth.jsx'

class LoginForm extends React.Component {
    state = {
        elements: {
            login: {
                value: "",
                error: null,
            },
            password: {
                value: "",
                icon: {
                    type: 'password',
                    name: 'eye slash',
                    show: false
                }
            }
        },
        loading: false,
        isValid: false,
    }

    componentDidMount() {
        document.title = `Вход - ${APP_NAME}`
    }

    /**
     * Переключение между скрытым и открытым вводом пароля
     */
    passwordIconHandler = _ => {
        const elements = { ...this.state.elements }
        const password = { ...elements.password }

        password.icon.show = !password.icon.show
        password.icon.type = password.icon.show ? 'text' : 'password'
        password.icon.name = password.icon.show ? 'eye' : 'eye slash'

        elements.password = password

        this.setState({ elements: elements })
    }

    /**
     * Изменение полей ввода формы
     */
    inputChangeHandler = (_, { name, value }) => {
        const elements = { ...this.state.elements }

        elements[name].value = value
        elements[name].error = null

        const isValid = elements.login.value && elements.password.value

        this.setState({
            elements: elements,
            isValid: isValid,
        })
    }

    /**
     * Инициирование формы
     */
    formSubmitHandler = event => {
        event.preventDefault()

        this.setState({ loading: true })

        const elements = { ...this.state.elements }

        const timezoneOffset = new Date().getTimezoneOffset() / 60
        const timezone = timezoneOffset > 0 ? `+${timezoneOffset}` : timezoneOffset

        axios.post('/login', {
            timezone: timezone,
            login: elements.login.value,
            password: elements.password.value,
        })
            .then(({ data }) => {
                window.location.href = data.intended || '/'
            })
            .catch(error => {
                if (error.response.status === 422) {
                    const errors = error.response.data.errors

                    if (errors) {
                        Object.keys(errors).forEach(key => {
                            const element = elements[key]

                            element.valid = false
                            element.error = errors[key][0]

                            elements[key] = element
                        })
                    }
                }

                this.setState({
                    elements: elements,
                    isValid: false,
                    loading: false,
                })
            })
    }

    render() {
        const { elements, isValid, loading } = this.state
        const { login, password } = elements

        return (
            <Grid.Column style={{ maxWidth: "400px" }}>
                <Segment>
                    <Form>
                        <div
                            style={{ textAlign: "center" }}>
                            <label style={{ fontSize: "16px", fontWeight: "bold", textTransform: "uppercase" }}>
                                Вход
                            </label>
                        </div>

                        <Divider />

                        <Form.Input
                            name="login"
                            label="Логин"
                            value={login.value}
                            placeholder="Логин"
                            autoComplete="off"
                            error={login.error ? { content: login.error } : null}
                            onChange={this.inputChangeHandler} />

                        <Form.Input
                            name="password"
                            label="Пароль"
                            type={password.icon.type}
                            value={password.value}
                            placeholder="******"
                            icon={
                                <Icon
                                    link
                                    name={password.icon.name}
                                    onClick={this.passwordIconHandler} />
                            }
                            autoComplete="off"
                            onChange={this.inputChangeHandler} />

                        <Divider />

                        <Form.Button
                            type="submit"
                            primary
                            disabled={!isValid || loading}
                            loading={loading}
                            onClick={this.formSubmitHandler}
                        >
                            Войти
                        </Form.Button>
                    </Form>
                </Segment>
            </Grid.Column>
        )
    }
}

export default LoginForm
