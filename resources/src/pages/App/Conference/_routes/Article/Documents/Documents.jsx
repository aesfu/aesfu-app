/**
 * Приложенные документы к статье
 */
import React from 'react'
import { Table, Button, Label } from 'semantic-ui-react'
import CloudUpload from '@material-ui/icons/CloudUploadOutlined'
import Delete from '@material-ui/icons/Delete'

import './Documents.scss'

/** Ограничение размера загружаемых документов */
const MAX_FILE_SIZE_MB = 5
const MAX_FILE_SIZE_B = MAX_FILE_SIZE_MB * 1024 * 1024

class Documents extends React.Component {
    componentDidMount() {
        this.dialog = document.querySelector('input#dialog')
        this.openDialog = document.querySelector('.open-dialog')

        document.addEventListener('dragover', this.dragoverHandler, false)
        document.addEventListener('dragleave', this.dragleaveHandler, false)
        document.addEventListener('drop', this.dropHandler, false)
    }

    componentWillUnmount() {
        document.removeEventListener('dragover', this.dragoverHandler)
        document.removeEventListener('dragleave', this.dragleaveHandler)
        document.removeEventListener('drop', this.dropHandler)
    }

    removeDocumentHandler = index => {
        let documents = [ ...this.props.documents ]

        documents.splice(index, 1)

        this.props.setDocuments(documents)
    }

    render() {
        const { documents, documentsError } = this.props

        const errorMessage = !documentsError ? null : (
            <div style={{ textAlign: "center" }}>
                <Label basic color="red" pointing style={{ marginTop: "4px" }}>
                    { documentsError }
                </Label>
            </div>
        )

        const documentsTable = documents.length === 0 ? null : (
            <Table basic="very" compact unstackable >
                <Table.Header>
                    <Table.Row>
                        <Table.HeaderCell width={ 1 } textAlign="center">#</Table.HeaderCell>
                        <Table.HeaderCell width={ 14 }>Наименование файла</Table.HeaderCell>
                        <Table.HeaderCell width={ 1 } textAlign="center"></Table.HeaderCell>
                    </Table.Row>
                </Table.Header>

                <Table.Body>
                    {
                        documents.map((elem, index) => (
                            <Table.Row key={ index }>
                                <Table.Cell textAlign="center">{ index + 1 }</Table.Cell>
                                <Table.Cell>{ elem.name }</Table.Cell>
                                <Table.Cell textAlign="center">
                                    <Button
                                        basic
                                        compact
                                        negative
                                        icon={ <Delete style={{ fontSize: "16px", verticalAlign: "bottom" }} /> }
                                        onClick={ _ => this.removeDocumentHandler(index) } />
                                </Table.Cell>
                            </Table.Row>
                        ))
                    }
                </Table.Body>
            </Table>
        )

        return (
            <>
                <div className="open-dialog" onClick={ _ => this.dialog.click() }>
                    <input
                        id="dialog"
                        type="file"
                        onChange={ event => this.processDialog(event.target.files) } />

                    <div className="open-dialog__info">
                        <CloudUpload style={{ fontSize: "64px" }} />

                        <p>
                            <label>Перетащите документы сюда или откройте диалоговое окно</label>
                            <label>(допускается загрузка не более 5 фалов)</label>
                        </p>
                    </div>
                </div>

                { errorMessage }

                { documentsTable }
            </>
        )
    }

    dragoverHandler = event => {
        event.preventDefault()
        this.openDialog?.classList.add('open-dialog--active')
    }

    dragleaveHandler = () => {
        this.openDialog?.classList.remove('open-dialog--active')
    }

    dropHandler = event => {
        event.preventDefault()
        this.openDialog?.classList.remove('open-dialog--active')

        this.processDialog(event.dataTransfer.files)
    }

    /**
     * Обработка открытых документов
     */
    processDialog = files => {
        let documents = [ ...this.props.documents ]

        for (let file of files) {
            const isAdded = documents.some(elem => elem.name === file.name)

            if (isAdded || file.type === '') {
                continue
            }

            if (file.size > MAX_FILE_SIZE_B) {
                alert(`Объем документа '${file.name}' превысил допустимый размер в ${MAX_FILE_SIZE_MB} МБ`)
                continue
            }

            documents.push(file)
        }

        if (documents.length > 5) {
            documents = documents.slice(0, 5)
        }

        this.props.setDocuments(documents)
    }
}

export default Documents
