/**
 * Приглашение на участие в конференции
 */
import React from 'react'
import { Grid, Segment, Divider } from 'semantic-ui-react'
import LinearProgress from '@material-ui/core/LinearProgress'
import { withRouter } from 'react-router-dom'
import axios from 'utils/axios.jsx'

import { APP_NAME } from 'utils/auth.jsx'
import queryString from 'utils/queryString.jsx'

import Links from './Links/Links.jsx'
import PlaceholderContent from 'app/_components/PlaceholderContent/PlaceholderContent.jsx'

import './Appeal.scss'

/** Диапазон лет отображения конференций */
const MIN_YEAR = 2020
const MAX_YEAR = 2024

class Appeal extends React.Component {
    constructor() {
        super()

        const wls = queryString()

        let year = parseInt(wls.year) || MAX_YEAR
        year = year >= MIN_YEAR && year <= MAX_YEAR ? year : MAX_YEAR

        this.state = {
            year: year,
            view: null,
            views: {},
            loading: true,
        }
    }

    componentDidMount() {
        const { year } = this.state

        document.title = `РИИ-${year} - ${APP_NAME}`

        this.loadAppeal()
    }

    /**
     * Смена года проведения конференции
     */
    appealClickHandler = value => {
        const { loading } = this.state

        if (loading) return

        document.title = `РИИ-${value} - ${APP_NAME}`

        this.props.history.push(`/conf?year=${value}`)

        this.setState({ year: value }, () => this.loadAppeal())
    }

    render() {
        const { year, view, loading } = this.state

        const links = (
            <Links
                year={year}
                loading={loading}
                show={{ left: year !== MIN_YEAR, right: year !== MAX_YEAR }}
                appealClick={this.appealClickHandler} />
        )

        const loader = view && loading ? (
            <LinearProgress style={{ margin: '1rem 0', height: '2px' }} />
        ) : (
            <Divider />
        )

        const content = !view ? (
            <PlaceholderContent fluid />
        ) : (
            <div className="ap" dangerouslySetInnerHTML={{ __html: view }}></div>
        )

        return (
            <Grid.Row centered columns={1}>
                <Grid.Column style={{ maxWidth: "800px" }}>
                    <Segment>
                        {links}
                        {loader}

                        {content}

                        {loader}
                        {links}
                    </Segment>
                </Grid.Column>
            </Grid.Row>
        )
    }

    /**
     * Загрузка приглашения на конференцию
     */
    loadAppeal = () => {
        const { year, views } = this.state

        if (views[year]) {
            this.setState({ view: views[year] })

            return
        }

        this.setState({ loading: true })

        axios.get(`/api/guest/conf/appeal?year=${year}`)
            .then(({ data }) => {
                views[year] = data

                this.setState({
                    view: data || null,
                    views: views,
                    loading: false,
                })
            }).catch(() => { })
    }
}

export default withRouter(Appeal)
