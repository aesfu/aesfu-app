/**
 * Новостная лента
 */
import React from 'react'
import { NavLink } from 'react-router-dom'
import { Card } from 'semantic-ui-react'

const feeds = [
    {
        description:
            <>
                <div style={{ textAlign: 'center', fontSize: '1.2rem', fontWeight: 700, paddingBottom: '14px' }}>
                    <span>Добро пожаловать на AESU!</span>
                </div>
                <div style={{ textAlign: "justify" }}>
                    <p>
                        Ресурс AESU предназначен для организации сопровождения{' '}
                        научно-образовательной деятельности. Сервисы сайта позволяют организовать{' '}
                        проведение контрольно-измерительных мероприятий, накопление и визуализацию{' '}
                        цифрового образовательного следа.
                    </p>
                    <p>
                        На ресурсе так же размещены научно-методические материалы и статьи,{' '}
                        опубликованные в рамках ежегодной конференции{' '}
                        «
                        {/* <NavLink to="/conf" title="Робототехника и искусственный интеллект"> */}
                            Робототехника и искусственный интеллект
                        {/* </NavLink> */}
                        ».
                    </p>
                </div>
            </>,
    },
    {
        description:
            <p style={{ textAlign: "justify" }}>
                Приглашаем Вас принять участие в <b>XVI Всероссийской{' '}
                научно-технической конференции с международным участием{' '}
                «Робототехника и искусственный интеллект» (РИИ-24)</b>,{' '}
                посвящённой вопросам развития общей и специальной робототехники,{' '}
                а также интеллектуальным системам. Конференция будет проводиться{' '}
                <b>30 ноября 2024 г.</b> Сибирским федеральным университетом (г. Железногорск Красноярского края).
            </p>,
        extra:
            <div>
                <NavLink to="/conf" className="ui primary basic small button">Принять участие</NavLink>
            </div>,
        hidden: false,
    },
    {
        description:
            <>
                <p style={{ textAlign: "justify" }}>
                    Найдены ошибки? <u>Забыт пароль</u>? - обращайся к своему преподавателю или{' '}
                    пиши на почту '<i className="user-select">aesfu@yandex.ru</i>'
                </p>
                <p>С уважением разработчики!</p>
            </>,
    },
].reduce((red, elem, index) => {
    if (elem.hidden) return red

    red.push(
        <Card key={ index } style={{ borderRadius: "0" }}>
            {
                Boolean(elem.header) &&
                <Card.Content header={ elem.header } />
            }

            <Card.Content description={ elem.description } />

            {
                Boolean(elem.extra) &&
                <Card.Content extra>{ elem.extra }</Card.Content>
            }
        </Card>
    )

    return red
}, [])

const HomeFeeds = () => (
    <Card.Group itemsPerRow={1}>
        { feeds }
    </Card.Group>
)

export default HomeFeeds
