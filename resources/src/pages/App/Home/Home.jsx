/**
 * Домашняя страница
 */
import React from 'react'
import { Grid } from 'semantic-ui-react'

import { APP_NAME } from 'utils/auth.jsx'
import HomeFeeds from './HomeFeeds/HomeFeeds.jsx'

const Home = () => {
    document.title = `Главная - ${APP_NAME}`

    return (
        <Grid stackable>
            <Grid.Row centered columns={1} style={{ maxWidth: "800px" }}>
                <Grid.Column>
                    <HomeFeeds />
                </Grid.Column>
            </Grid.Row>
        </Grid>
    )
}

export default Home
