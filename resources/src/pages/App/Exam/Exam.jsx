/**
 * Страница тестов
 */
import React, { Suspense } from 'react'
import { Grid } from 'semantic-ui-react'
import { Route, Switch } from 'react-router-dom'

import PlaceholderContent from 'app/_components/PlaceholderContent/PlaceholderContent.jsx'

const UserTests = React.lazy(() => import('./_routes/UserTests/UserTests'))
const Examination = React.lazy(() => import('./_routes/Examination/Examination'))

const Exam = () => (
    <Grid stackable>
        <Grid.Row centered columns={1} style={{ maxWidth: "800px" }}>
            <Suspense fallback={<PlaceholderContent fluid />}>
                <Switch>
                    <Route exact path="/tests" render={() => <UserTests />} />
                    <Route exact path="/test/:id" render={props => <Examination {...props} />} />
                </Switch>
            </Suspense>
        </Grid.Row>
    </Grid>
)

export default Exam
