/**
 * Отображение одного тестового задания
 */
import React, { Fragment } from 'react'
import { Form, Checkbox, Label } from 'semantic-ui-react'

const Question = props => {
    const { quest, values, checkChanged, radioChanged, inputChanged, rangeChanged } = props

    let answers = quest.answers.sort((a, b) => a.position - b.position)

    /** Один вариант ответа и множественный выбор */
    if ([0, 1, 2].includes(quest.quest_type)) {
        answers = answers.map((elem, index) => (
            <Form.Field key={ index }>
                <Checkbox
                    radio={ [1, 2].includes(quest.quest_type) }
                    label={ elem.name }
                    value={ elem.id }
                    checked={ values.includes(elem.id) }
                    onChange={ quest.quest_type === 0 ? checkChanged : radioChanged } />
            </Form.Field>
        ))
    }
    /** Свободная формулировка ответа */
    else if (quest.quest_type === 3) {
        answers = (
            <Form.Field>
                <Form.Input
                    type="text"
                    value={ values }
                    onChange={ inputChanged } />
            </Form.Field>
        )
    }
    /** Выбор в диапазоне */
    else if (quest.quest_type === 4) {
        answers = answers.map((elem, index) => (
            <Form.Field key={ index }>
                <label>{ elem.name }</label>

                <div style={{ display: "flex", alignItems: "center" }}>
                    <input
                        type="range"
                        value={ values[index] }
                        min={ elem.min }
                        max={ elem.max }
                        onChange={ event => rangeChanged(event, index) } />

                    <Label
                        basic
                        circular
                        style={{ minWidth: "35px", marginLeft: "8px" }}
                    >
                        { values[index] }
                    </Label>
                </div>
            </Form.Field>
        ))
    }

    return (
        <Fragment>
            <Form.Field>
                <label style={{ fontSize: "16px", marginBottom: "15px" }}>
                    { props.quest.name }
                </label>
            </Form.Field>

            { answers }
        </Fragment>
    )
}

export default Question
