/**
 * Страница профиля пользователя
 */
import React from 'react'
import { Route, Switch } from 'react-router-dom'
import { Grid, Segment } from 'semantic-ui-react'

import 'app/_styles/Sidebar/Sidebar.scss'

import Menu from './Menu/Menu.jsx'
import ProfileMain from './_routes/ProfileMain.jsx'
import ProfileInfo from './_routes/ProfileInfo.jsx'
import ProfileBranch from './_routes/ProfileBranch.jsx'
import ProfileEmail from './_routes/ProfileEmail.jsx'
import ProfilePassword from './_routes/ProfilePassword.jsx'
import ProfileBasket from './_routes/ProfileBasket.jsx'

class Profile extends React.Component {
    componentDidMount() {
        this.windowResize = () => this.toggleHandler(false)

        const links = document.querySelectorAll('.sidebar-menu__item')
        for (let link of links) link.addEventListener('click', () => this.toggleHandler())

        document.querySelector('.content-dimmer').addEventListener('click', () => this.toggleHandler())

        window.addEventListener('resize', this.windowResize)
    }

    componentWillUnmount() {
        window.removeEventListener('resize', this.windowResize)
    }

    /**
     * Состояния отображения бокового меню (mobile & tablet)
     */
    toggleHandler = value => {
        const dimmer = document.querySelector('.content-dimmer')
        const content = document.querySelector('.nav-sidebar')

        value ? dimmer?.classList.remove('content-dimmer--collapsed') :
            dimmer?.classList.add('content-dimmer--collapsed')

        value ? content?.classList.add('nav-sidebar--expand') :
            content?.classList.remove('nav-sidebar--expand')
    }

    render() {
        return (
            <Grid stackable>
                <Grid.Row>
                    <div className="nav-sidebar">
                        <Menu />
                    </div>

                    <Grid.Column style={{ flexGrow: "1" }}>
                        <div className="content-dimmer content-dimmer--collapsed"></div>

                        <Segment style={{ margin: 0 }}>
                            <Switch>
                                <Route exact path="/profile" render={() => <ProfileMain onToggle={this.toggleHandler} />} />
                                <Route exact path="/profile/info" render={() => <ProfileInfo onToggle={this.toggleHandler} />} />
                                <Route exact path="/profile/branch" render={() => <ProfileBranch onToggle={this.toggleHandler} />} />
                                <Route exact path="/profile/email" render={() => <ProfileEmail onToggle={this.toggleHandler} />} />
                                <Route exact path="/profile/password" render={() => <ProfilePassword onToggle={this.toggleHandler} />} />
                                <Route exact path="/profile/basket" render={() => <ProfileBasket onToggle={this.toggleHandler} />} />
                            </Switch>
                        </Segment>
                    </Grid.Column>
                </Grid.Row>
            </Grid>
        )
    }
}

export default Profile
