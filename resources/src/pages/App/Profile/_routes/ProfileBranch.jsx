/**
 * Настройки факультативной группы
 */
import React from 'react'
import { connect } from 'react-redux'
import { NavLink } from 'react-router-dom'
import { Grid, Form, Breadcrumb, Icon, Divider } from 'semantic-ui-react'
import axios from 'utils/axios.jsx'

import auth, { APP_NAME } from 'utils/auth.jsx'
import store, { TestsTypes, ProfileTypes } from 'store/store.jsx'

class ProfileBranch extends React.Component {
    state = {
        elements: {
            branch: {
                value: 0,
                loading: true
            }
        },
        loading: false,
        valid: false
    }

    componentDidMount() {
        document.title = `Группа - ${APP_NAME}`

        const { branches } = this.props.profile

        if (branches.loaded) {
            this.setState(state => ({
                elements: {
                    ...state.elements,
                    branch: {
                        value: auth().branch ? auth().branch.id : null,
                        loading: false,
                    }
                }
            }))
        }
        else {
            axios.post('/api/branches?sort=d')
                .then(({ data }) => {
                    const options = data.map(elem => ({
                        key: elem.id,
                        value: elem.id,
                        text: elem.name,
                    }))

                    store.dispatch(ProfileTypes.branches(options))

                    this.setState(state => ({
                        elements: {
                            ...state.elements,
                            branch: {
                                value: auth().branch ? auth().branch.id : null,
                                loading: false,
                            }
                        }
                    }))
                }).catch(() => { })
        }
    }

    /**
     * Выбор факультативной группы
     */
    dropdownChangeHandler = (_, { value }) => {
        const elements = { ...this.state.elements }

        elements.branch.value = value

        this.setState({
            elements: elements,
            valid: this.isValid(elements)
        })
    }

    /**
     * Инициирование формы
     */
    submitFormHandler = _ => {
        this.setState({ loading: true })

        const { elements } = this.state

        const data = {
            branchId: elements.branch.value,
        }

        axios.put('/api/auth/branch', data)
            .then(({ data }) => {
                if (data.result) {
                    auth().branch = data.branch
                    store.dispatch(TestsTypes.tests([], false))
                }

                this.setState({
                    loading: false,
                    valid: false
                })
            })
            .catch(({ response }) => {
                const errors = response.data.errors

                const keys = Object.keys(errors)

                if (keys.length) {
                    for (let key of keys) {
                        elements[key].error = errors[key]
                        elements[key].valid = false
                    }

                    this.setState({ loading: false, elements: elements })
                }
            })
    }

    render() {
        const { elements, loading, valid } = this.state
        const { branches } = this.props.profile

        return (
            <Grid.Row columns={1}>
                <Grid.Column verticalAlign="middle">
                    <div className="button-toggle" onClick={this.props.onToggle}><Icon name="bars" /></div>

                    <Breadcrumb
                        size="tiny"
                        icon="right angle"
                        sections={[
                            { key: 0, content: 'Профиль', to: '/profile', as: (NavLink), activeClassName: '' },
                            { key: 1, content: 'Группа', link: false },
                        ]} />
                </Grid.Column>

                <Divider />

                <Grid.Column>
                    <Form>
                        <Form.Dropdown
                            fluid
                            search
                            selection
                            clearable
                            selectOnBlur={false}
                            label="Факультативная группа"
                            placeholder="Выбор группы"
                            loading={elements.branch.loading}
                            value={elements.branch.value}
                            options={branches.data}
                            onChange={this.dropdownChangeHandler} />

                        <Grid columns={1}>
                            <Grid.Column textAlign="left">
                                <Form.Button
                                    primary
                                    size="small"
                                    type="submit"
                                    loading={loading}
                                    disabled={!valid || loading}
                                    onClick={this.submitFormHandler}
                                >
                                    Сохранить
                                </Form.Button>
                            </Grid.Column>
                        </Grid>
                    </Form>
                </Grid.Column>
            </Grid.Row>
        )
    }

    /**
     * Проверка верности заполнения формы
     */
    isValid = elements => {
        const branch_id = auth().branch ? auth().branch.id : 0

        return elements.branch.value != branch_id
    }
}

const mapStateToProps = state => ({ profile: state.profile })

export default connect(mapStateToProps)(ProfileBranch)
