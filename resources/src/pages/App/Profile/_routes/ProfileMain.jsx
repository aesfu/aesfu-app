/**
 * Отображение основной информации пользователя
 */
import React from 'react'
import { Grid, Table, Breadcrumb, Icon, Divider } from 'semantic-ui-react'
import dayjs from 'dayjs'

import auth, { APP_NAME } from 'utils/auth.jsx'

class ProfileMain extends React.Component {
    state = {
        FIO: `${auth().surname} ${auth().name} ${auth().patronym}`,
        login: auth().login,
        status: auth().userstatus.name,
        branch: auth().branch ? auth().branch.name : '-',
        speciality: auth().branch ? auth().branch.speciality.name : '-',
        email: auth().email ? auth().email : '-',
        created_at: dayjs(auth().created_at),
    }

    componentDidMount() {
        document.title = `Профиль - ${APP_NAME}`
    }

    render() {
        const { FIO, login, status, branch, speciality, email, created_at } = this.state

        return (
            <Grid.Row columns={1}>
                <Grid.Column verticalAlign="middle">
                    <div className="button-toggle" onClick={this.props.onToggle}><Icon name="bars" /></div>

                    <Breadcrumb
                        size="tiny"
                        icon="right angle"
                        sections={[
                            { key: 0, content: 'Профиль', link: false },
                        ]} />
                </Grid.Column>

                <Divider />

                <Grid.Column>
                    <Table basic="very" unstackable compact style={{ marginTop: "0" }}>
                        <Table.Body>
                            <Table.Row>
                                <Table.Cell style={{ width: "145px", borderColor: "transparent" }} ><b>ФИО</b></Table.Cell>
                                <Table.Cell style={{ borderColor: "transparent" }}>{FIO}</Table.Cell>
                            </Table.Row>

                            <Table.Row>
                                <Table.Cell style={{ width: "145px", borderColor: "transparent" }} ><b>Логин</b></Table.Cell>
                                <Table.Cell style={{ borderColor: "transparent" }}>{login}</Table.Cell>
                            </Table.Row>

                            <Table.Row>
                                <Table.Cell style={{ width: "145px", borderColor: "transparent" }} ><b>Статус</b></Table.Cell>
                                <Table.Cell style={{ borderColor: "transparent" }}>{status}</Table.Cell>
                            </Table.Row>

                            <Table.Row>
                                <Table.Cell style={{ width: "145px", borderColor: "transparent" }} ><b>Группа</b></Table.Cell>
                                <Table.Cell style={{ borderColor: "transparent" }}>{branch}</Table.Cell>
                            </Table.Row>

                            <Table.Row>
                                <Table.Cell style={{ width: "145px", borderColor: "transparent" }} ><b>Специальность</b></Table.Cell>
                                <Table.Cell style={{ borderColor: "transparent" }}>{speciality}</Table.Cell>
                            </Table.Row>

                            <Table.Row>
                                <Table.Cell style={{ width: "145px", borderColor: "transparent" }} ><b>Email-адрес</b></Table.Cell>
                                <Table.Cell style={{ borderColor: "transparent" }}>{email}</Table.Cell>
                            </Table.Row>

                            <Table.Row>
                                <Table.Cell style={{ width: "145px", borderColor: "transparent" }} ><b>Создан</b></Table.Cell>
                                <Table.Cell style={{ borderColor: "transparent" }}>{created_at.format('DD.MM.YYYY H:m:s')}</Table.Cell>
                            </Table.Row>
                        </Table.Body>
                    </Table>
                </Grid.Column>
            </Grid.Row>
        )
    }
}

export default ProfileMain
