/**
 * Вывод системных сообщений
 */
import React from 'react'
import { connect } from 'react-redux'
import { Message } from 'semantic-ui-react'

import store, { MessageTypes } from 'store/store.jsx'

const MessageBox = props => {
    const { format, header, message } = props.message

    if (!message) return (null)

    window.scrollTo(0, 0)

    return (
        <div style={{ display: 'flex', justifyContent: 'center', padding: '14px 0' }}>
            <Message
                title="Закрыть"
                info={format === 'info'}
                warning={format === 'warning'}
                positive={format === 'positive'}
                negative={format === 'negative'}
                style={{ maxWidth: '500px', flexGrow: '1', borderRadius: '0', cursor: 'pointer' }}
                onClick={() => {
                    if (window.getSelection().type != 'Range') {
                        store.dispatch(MessageTypes.box({ message: '' }))
                    }
                }}
            >

                {
                    header &&
                    (
                        <Message.Header style={{ marginBottom: '5px' }}>
                            {header}
                        </Message.Header>
                    )
                }

                <span style={{ cursor: 'text' }}>
                    {message}
                </span>
            </Message>
        </div>
    )
}

const mapStateToProps = state => ({ message: state.message })

export default connect(mapStateToProps)(MessageBox)
